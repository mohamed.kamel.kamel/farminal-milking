/**
 * Automatically generated file. DO NOT MODIFY
 */
package org.wasila.farminalmilking;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "org.wasila.farminalmilking";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 21;
  public static final String VERSION_NAME = "2.1";
}
