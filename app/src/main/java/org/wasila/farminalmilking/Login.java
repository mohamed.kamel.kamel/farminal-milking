package org.wasila.farminalmilking;

import org.greenrobot.greendao.query.CloseableListIterator;
import org.greenrobot.greendao.query.Query;
import org.json.JSONObject;
import org.wasila.farminalmilking.caching.Credentials;
import org.wasila.farminalmilking.caching.CredentialsDao;
import org.wasila.farminalmilking.engine.API;
import org.wasila.farminalmilking.engine.webservice;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Login extends SuperActivity {
    EditText uname_login, pass_login;
    String uname, upass;
    Button login_btn;
    Context myContext = null;

    @BindView(R.id.settings_btn)
    ImageButton settings_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getDomainName();
        String[] userCredentials = getSavedUsernameAndPass();
        uname = userCredentials[0];
        upass = userCredentials[1];

        if (TextUtils.isEmpty(webservice.domain_name))
            webservice.domain_name = webservice.default_domain_name;

        if (uname == null || upass == null) {
            drawLayout();
        } else {
            new login_task().execute();
        }
    }

    class login_task extends AsyncTask<Void, Void, Void> {
        String res;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            res = new API().login(uname, upass);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            JSONObject jsonObject;
            try {
                jsonObject = new JSONObject(res);
                if (jsonObject.getInt("res") == -1) {
                    drawLayout();
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            Login.this);
                    builder.setMessage(
                            "login failed please check the username and password")
                            .setCancelable(true);
                    builder.setPositiveButton("ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else if (!jsonObject.getString("key").equals(
                        "HAKMSNKJ&%^$#HKSSS@123!")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            Login.this);
                    builder.setMessage("License has been expired !!").setCancelable(
                            true);
                    builder.setPositiveButton("أخفاء",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    dialog.cancel();
                                    finish();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    Intent i = new Intent(Login.this, Menu.class);
                    startActivity(i);
                    finish();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public String[] getSavedUsernameAndPass() {
        this.openDb();
        ////////////////////////////////////////
        CredentialsDao credentialsDao = daoSession.getCredentialsDao();
        String[] userCredentials = new String[2];

        Query<Credentials> levelQueryList = credentialsDao.queryBuilder().where(CredentialsDao.Properties.Privilege.eq(1)).build();
        for (CloseableListIterator<Credentials> it = levelQueryList.listIterator(); it.hasNext(); ) {
            Credentials credentials = it.next();
            userCredentials[0] = credentials.getUsername();
            userCredentials[1] = credentials.getPass();
        }
        return userCredentials;
    }

    private void drawLayout() {
        setContentView(R.layout.login);
        ButterKnife.bind(this);
        uname_login = (EditText) findViewById(R.id.usernameEdit_login);
        pass_login = (EditText) findViewById(R.id.passwordEdit_login);
        login_btn = (Button) findViewById(R.id.login_btn);
        login_btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                uname = uname_login.getText().toString();
                upass = pass_login.getText().toString();
                if (uname.equals("") || upass.equals("")) {
                    Toast.makeText(
                            getApplicationContext(),
                            getResources().getString(
                                    R.string.ErrorLoginAr),
                            Toast.LENGTH_SHORT).show();
                } else {
                    new login_task().execute();
                }
            }
        });
    }

    public void handleSaveIp(View view) {
        startActivity(new Intent(getApplicationContext(), MainSettings.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

}
