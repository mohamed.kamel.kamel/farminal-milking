package org.wasila.farminalmilking;

import org.wasila.farminalmilking.engine.API;
import org.wasila.farminalmilking.engine.API_Local;
import org.wasila.farminalmilking.engine.webservice;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Testing extends SuperActivity {
    Button qualityBtn;
    EditText nonfatText, fatText, ProteinText, SCCText, AflatoxinText;
    DatePicker milkTestdatepicker;
    TextView milkTestText;
    String lang;
    Context myContext = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quality);
        lang = getLang();
        qualityBtn = (Button) findViewById(R.id.qualityBtn);
        nonfatText = (EditText) findViewById(R.id.nonfatText);
        fatText = (EditText) findViewById(R.id.fatText);
        ProteinText = (EditText) findViewById(R.id.ProteinText);
        AflatoxinText = (EditText) findViewById(R.id.AflatoxinText);
        SCCText = (EditText) findViewById(R.id.SCCText);
        milkTestdatepicker = (DatePicker) findViewById(R.id.milkTestdatepicker);
        milkTestText = (TextView) findViewById(R.id.milkTestText);
        // /////////////////////////////////////////////////////
        if (lang.equals("En")) {
            qualityBtn.setText(getResources().getString(R.string.saveEn));
            milkTestText.setText(getResources().getString(
                    R.string.milkTestDateEn));
            nonfatText.setHint(getResources().getString(R.string.SolidsEn));
            fatText.setHint(getResources().getString(R.string.fatEn));
            ProteinText.setHint(getResources().getString(R.string.ProteinEn));
            AflatoxinText.setHint(getResources()
                    .getString(R.string.AflatoxinEn));
            SCCText.setHint(getResources().getString(R.string.SCCEn));
        }
        // /////////////////////////////////////////////////////
        qualityBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!nonfatText.getText().equals("")
                        && !fatText.getText().equals("")) {
                    if (new webservice().isNetworkAvailable(Testing.this)) {
                        new sendTest().execute();
                    } else {
                        int year = milkTestdatepicker.getYear();
                        int month = milkTestdatepicker.getMonth() + 1;
                        int day = milkTestdatepicker.getDayOfMonth();
                        new API_Local(Testing.this, lang).addTesting(year + "/"
                                        + month + "/" + day, fatText.getText()
                                        .toString(), nonfatText.getText().toString(),
                                SCCText.getText().toString(), AflatoxinText
                                        .getText().toString(), ProteinText
                                        .getText().toString());
                        if (lang.equals("Ar")) {
                            Toast.makeText(
                                    getApplicationContext(),
                                    getResources().getString(R.string.saveOkAr),
                                    Toast.LENGTH_LONG).show();
                        } else if (lang.equals("En")) {
                            Toast.makeText(
                                    getApplicationContext(),
                                    getResources().getString(R.string.saveOkEn),
                                    Toast.LENGTH_LONG).show();
                        }
                        Intent i = new Intent(Testing.this, Menu.class);
                        startActivity(i);
                        finish();
                    }
                }
            }
        });
    }

    class sendTest extends AsyncTask<String, Void, Void> {
        int res;
        ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if (lang.equals("Ar")) {
                pDialog = new ProgressDialog(Testing.this);
                pDialog.setMessage(getResources().getString(R.string.LoadingAr));
                pDialog.show();
            } else if (lang.equals("En")) {
                pDialog = new ProgressDialog(Testing.this);
                pDialog.setMessage(getResources().getString(R.string.LoadingEn));
                pDialog.show();
            }
        }

        @Override
        protected Void doInBackground(String... params) {
            int year = milkTestdatepicker.getYear();
            int month = milkTestdatepicker.getMonth() + 1;
            int day = milkTestdatepicker.getDayOfMonth();
            res = new API().insert_testing(year + "/" + month + "/" + day,
                    fatText.getText().toString(), nonfatText.getText()
                            .toString(), ProteinText.getText().toString(),
                    SCCText.getText().toString(), AflatoxinText.getText()
                            .toString());
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            pDialog.dismiss();
            if (res == -1) {
                if (lang.equals("Ar")) {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.errorInputAr),
                            Toast.LENGTH_LONG).show();
                } else if (lang.equals("En")) {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.errorInputEn),
                            Toast.LENGTH_LONG).show();
                }
            } else {
                if (lang.equals("Ar")) {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.saveOkAr),
                            Toast.LENGTH_LONG).show();
                } else if (lang.equals("En")) {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.saveOkEn),
                            Toast.LENGTH_LONG).show();
                }
                Intent i = new Intent(Testing.this, Menu.class);
                startActivity(i);
                finish();
            }
        }
    }
}
