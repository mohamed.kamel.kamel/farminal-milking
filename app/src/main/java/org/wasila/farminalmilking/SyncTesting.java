package org.wasila.farminalmilking;

import java.util.ArrayList;

import org.json.JSONObject;
import org.wasila.farminalmilking.engine.API;
import org.wasila.farminalmilking.engine.API_Local;
import org.wasila.farminalmilking.engine.SyncTestingRow;
import org.wasila.farminalmilking.engine.TestingInfo;
import org.wasila.farminalmilking.engine.webservice;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class SyncTesting extends SuperActivity implements OnItemClickListener {
	TextView login_title;
	EditText usernameEdit_login, passwordEdit_login;
	Button delete, sync;
	ListView records;
	String lang = "", uname, upass;
	Context myContext = null;
	ArrayList<TestingInfo> testing_arr = new ArrayList<TestingInfo>();
	ArrayList<String> deletePosition = new ArrayList<String>();

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, final int position,
			long arg3) {
		TextView calf_delete_checkbox = (TextView) arg1
				.findViewById(R.id.calf_delete_checkbox);
		if (deletePosition.contains(position + "")) {
			deletePosition.remove(position + "");
			calf_delete_checkbox.setCompoundDrawablesWithIntrinsicBounds(
					android.R.drawable.ic_menu_add, 0, 0, 0);
		} else {
			deletePosition.add(position + "");
			calf_delete_checkbox.setCompoundDrawablesWithIntrinsicBounds(
					android.R.drawable.ic_delete, 0, 0, 0);
		}
	}

	@SuppressLint("WrongConstant")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sync_milking_details);
		login_title = (TextView) findViewById(R.id.login_title);
		usernameEdit_login = (EditText) findViewById(R.id.usernameEdit_login);
		passwordEdit_login = (EditText) findViewById(R.id.passwordEdit_login);
		delete = (Button) findViewById(R.id.delete);
		sync = (Button) findViewById(R.id.sync);
		records = (ListView) findViewById(R.id.records);
		records.setOnItemClickListener(SyncTesting.this);
		// //////////////////////////////////////////////////////
		lang = getLang();
		// /////////////////////////////////////////////////////
		if (lang.equals("En")) {
			login_title
					.setText(getResources().getString(R.string.loginInputEn));
			usernameEdit_login.setHint(getResources().getString(
					R.string.usernameEn));
			passwordEdit_login.setHint(getResources().getString(
					R.string.passwordEn));
			delete.setText(getResources().getString(R.string.deleteEn));
			sync.setText(getResources().getString(R.string.syncEn));
		}
		new list_task().execute();
		// /////////////////////////////////////////////////////
		delete.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				for (String s : deletePosition) {
					int i = Integer.parseInt(s);
					new API_Local(SyncTesting.this, lang)
							.deleteTesting(testing_arr.get(i).get_test_date());
				}
				for (String s : deletePosition) {
					int i = Integer.parseInt(s);
					testing_arr.remove(i);
				}
				SyncTestingRow adapter = new SyncTestingRow(SyncTesting.this,
						testing_arr, lang);
				records.setAdapter(adapter);
				ViewGroup.LayoutParams params = records.getLayoutParams();
				params.height = (testing_arr.size()) * 205;
				records.setLayoutParams(params);
				records.requestLayout();
				deletePosition.clear();
			}
		});
		sync.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (new webservice().isNetworkAvailable(SyncTesting.this)) {
					uname = usernameEdit_login.getText().toString();
					upass = passwordEdit_login.getText().toString();
					if (uname.equals("") || upass.equals("")) {
						Toast.makeText(
								getApplicationContext(),
								getResources().getString(R.string.ErrorLoginAr),
								Toast.LENGTH_SHORT).show();
					} else {
						new login_task().execute();
					}
				} else {
					if (lang.equals("En")) {
						Toast.makeText(getApplicationContext(),
								"No internet connetion!!", Toast.LENGTH_SHORT)
								.show();
					} else {
						Toast.makeText(getApplicationContext(),
								"لا يوجد شبكة أنترنت !!", Toast.LENGTH_SHORT)
								.show();
					}
				}
			}
		});
	}

	class list_task extends AsyncTask<Void, Void, Void> {
		ProgressDialog pDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (lang.equals("Ar")) {
				pDialog = new ProgressDialog(SyncTesting.this);
				pDialog.setMessage(getResources().getString(R.string.LoadingAr));
				pDialog.show();
			} else if (lang.equals("En")) {
				pDialog = new ProgressDialog(SyncTesting.this);
				pDialog.setMessage(getResources().getString(R.string.LoadingEn));
				pDialog.show();
			}
		}

		@Override
		protected Void doInBackground(Void... params) {
			testing_arr = new API_Local(SyncTesting.this, lang)
					.selectTestingInfo();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			pDialog.dismiss();
			if (testing_arr.size() > 0) {
				SyncTestingRow adapter = new SyncTestingRow(SyncTesting.this,
						testing_arr, lang);
				records.setAdapter(adapter);
				ViewGroup.LayoutParams params = records.getLayoutParams();
				params.height = (testing_arr.size()) * 205;
				records.setLayoutParams(params);
				records.requestLayout();
			}
		}
	}

	class login_task extends AsyncTask<Void, Void, Void> {
		String res;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				res = new API().login(uname, upass);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(res);
				if (jsonObject.getInt("res") == -1) {
					AlertDialog.Builder builder = new AlertDialog.Builder(
							SyncTesting.this);
					builder.setMessage(
							"Login failed\ncheck username and password")
							.setCancelable(true);
					builder.setPositiveButton("ok",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.cancel();
								}
							});
					AlertDialog alert = builder.create();
					alert.show();
				} else {
					new add_task().execute();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	class add_task extends AsyncTask<Void, Void, Void> {
		int res;
		ProgressDialog pDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (lang.equals("Ar")) {
				pDialog = new ProgressDialog(SyncTesting.this);
				pDialog.setMessage(getResources().getString(R.string.LoadingAr));
				pDialog.show();
			} else if (lang.equals("En")) {
				pDialog = new ProgressDialog(SyncTesting.this);
				pDialog.setMessage(getResources().getString(R.string.LoadingEn));
				pDialog.show();
			}
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				for (TestingInfo ti : testing_arr) {
					res = new API().insert_testing(ti.get_test_date(),
							ti.get_fat(), ti.get_solid(), ti.get_Protein(),
							ti.get_SCC(), ti.get_Aflatoxin());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			pDialog.dismiss();
			try {
				if (res == -1) {
					AlertDialog.Builder builder = new AlertDialog.Builder(
							SyncTesting.this);
					if (lang.equals("En")) {
						builder.setMessage(
								getResources().getString(R.string.ErrorEn))
								.setCancelable(true);
						builder.setPositiveButton(
								getResources().getString(R.string.OKEn),
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();
									}
								});
						AlertDialog alert = builder.create();
						alert.show();
					} else if (lang.equals("Ar")) {
						builder.setMessage(
								getResources().getString(R.string.ErrorAr))
								.setCancelable(true);
						builder.setPositiveButton(
								getResources().getString(R.string.OkAr),
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();
									}
								});
						AlertDialog alert = builder.create();
						alert.show();
					}
				} else {
					if (lang.equals("En")) {
						Toast.makeText(getApplicationContext(),
								getResources().getString(R.string.savedataEn),
								Toast.LENGTH_LONG).show();
					} else if (lang.equals("Ar")) {
						Toast.makeText(getApplicationContext(),
								getResources().getString(R.string.savedataAr),
								Toast.LENGTH_LONG).show();
					}
					for (TestingInfo ti : testing_arr) {
						new API_Local(SyncTesting.this, lang).deleteTesting(ti
								.get_test_date());
					}
					Intent i = new Intent(SyncTesting.this, Menu.class);
					i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(i);
					finish();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
