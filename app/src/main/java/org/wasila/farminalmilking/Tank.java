package org.wasila.farminalmilking;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.wasila.farminalmilking.engine.API;
import org.wasila.farminalmilking.engine.API_Local;
import org.wasila.farminalmilking.engine.webservice;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class Tank extends SuperActivity {
	Button tankBtn;
	EditText milkValueText;
	CheckBox milkValueCheckbox;
	String lang;
	Context myContext = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tank);
		lang = getLang();
		tankBtn = (Button) findViewById(R.id.tankBtn);
		milkValueText = (EditText) findViewById(R.id.milkValueText);
		milkValueCheckbox = (CheckBox) findViewById(R.id.milkValueCheckbox);
		if (!new webservice().isNetworkAvailable(Tank.this)) {
			milkValueCheckbox.setVisibility(View.GONE);
		}
		// /////////////////////////////////////////////////////
		if (lang.equals("En")) {
			tankBtn.setText(getResources().getString(R.string.saveEn));
			milkValueCheckbox.setText(getResources().getString(
					R.string.milksellingen));
			milkValueText.setHint(getResources().getString(
					R.string.majorMilkTankEn));
		}
		// /////////////////////////////////////////////////////
		tankBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!milkValueText.getText().equals("")) {
					if (new webservice().isNetworkAvailable(Tank.this)) {
						new sendTankInfo().execute();
					} else {
						String milk_date_time = "";
						DateFormat format = new SimpleDateFormat(
								"yyyy/MM/dd HH:mm:ss",Locale.ENGLISH);
						Date date = new Date();
						milk_date_time = format.format(date);
						new API_Local(Tank.this, lang).addTank_value(
								milk_date_time, milkValueText.getText()
										.toString());
						if (lang.equals("Ar")) {
							Toast.makeText(
									getApplicationContext(),
									getResources().getString(R.string.saveOkAr),
									Toast.LENGTH_LONG).show();
						} else if (lang.equals("En")) {
							Toast.makeText(
									getApplicationContext(),
									getResources().getString(R.string.saveOkEn),
									Toast.LENGTH_LONG).show();
						}
						Intent i = new Intent(Tank.this, Menu.class);
						startActivity(i);
						finish();
					}
				}
			}
		});
	}

	class sendTankInfo extends AsyncTask<String, Void, Void> {
		int res;
		ProgressDialog pDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			if (lang.equals("Ar")) {
				pDialog = new ProgressDialog(Tank.this);
				pDialog.setMessage(getResources().getString(R.string.LoadingAr));
				pDialog.show();
			} else if (lang.equals("En")) {
				pDialog = new ProgressDialog(Tank.this);
				pDialog.setMessage(getResources().getString(R.string.LoadingEn));
				pDialog.show();
			}
		}

		@Override
		protected Void doInBackground(String... params) {
			int i = 0;
			if (milkValueCheckbox.isChecked()) {
				i = 1;
			}
			res = new API().insert_tank(milkValueText.getText().toString(), i);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			pDialog.dismiss();
			if (res == -1) {
				if (lang.equals("Ar")) {
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.errorInputAr),
							Toast.LENGTH_LONG).show();
				} else if (lang.equals("En")) {
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.errorInputEn),
							Toast.LENGTH_LONG).show();
				}
			} else {
				if (lang.equals("Ar")) {
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.saveOkAr),
							Toast.LENGTH_LONG).show();
				} else if (lang.equals("En")) {
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.saveOkEn),
							Toast.LENGTH_LONG).show();
				}
				Intent i = new Intent(Tank.this, Menu.class);
				startActivity(i);
				finish();
			}
		}
	}
}
