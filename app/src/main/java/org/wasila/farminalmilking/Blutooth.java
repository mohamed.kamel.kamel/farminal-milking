package org.wasila.farminalmilking;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.wasila.farminalmilking.engine.Tools;


import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.Toast;

public class Blutooth extends SuperActivity implements OnItemClickListener {
    private static List<BluetoothDevice> connectedDevices = new ArrayList<BluetoothDevice>();
    Handler mhandel = null;
    Switch buttonOn, buttonSearch;
    BluetoothDevice bdDevice;
    ListView listViewDetected;
    BluetoothAdapter bluetoothAdapter = null;
    ArrayList<BluetoothDevice> arrayListBluetoothDevices = new ArrayList<BluetoothDevice>();
    ArrayList<String> arrayListBluetoothDevicesName = new ArrayList<String>();
    private UUID MY_UUID = UUID
            .fromString("00001101-0000-1000-8000-00805F9B34FB");

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bluetoothcontainer);
        setResult(Activity.RESULT_CANCELED);
        buttonOn = (Switch) findViewById(R.id.buttonOn);
        buttonSearch = (Switch) findViewById(R.id.buttonSearch);
        listViewDetected = (ListView) findViewById(R.id.listViewDetected);
        if (!getBluetoothPermission()) {
            return;
        }
        listViewDetected.setOnItemClickListener(this);
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        buttonOn.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                if (isChecked) {
                    buttonSearch.setEnabled(true);
                    if (!bluetoothAdapter.isEnabled()) {
                        bluetoothAdapter.enable();
                    }
                } else {
                    buttonSearch.setEnabled(false);
                    if (bluetoothAdapter.isEnabled()) {
                        bluetoothAdapter.disable();
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                                Blutooth.this,
                                android.R.layout.simple_list_item_single_choice);
                        listViewDetected.setAdapter(adapter);
                    }
                }
            }
        });
        buttonSearch.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                if (isChecked) {
                    if (bluetoothAdapter.isDiscovering()) {
                        bluetoothAdapter.cancelDiscovery();
                    }
                    IntentFilter intentFilter = new IntentFilter(
                            BluetoothDevice.ACTION_FOUND);
                    Blutooth.this.registerReceiver(myReceiver, intentFilter);
                    bluetoothAdapter.startDiscovery();
                } else {
                    bluetoothAdapter.cancelDiscovery();
                }
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        for (BluetoothDevice bluetoothDevice : connectedDevices) {
            if (bluetoothDevice.getBondState() == BluetoothDevice.BOND_BONDED || bluetoothDevice.getBondState() == BluetoothDevice.BOND_BONDING) {
                Toast.makeText(Blutooth.this, "Already connect to a bluetooth device", Toast.LENGTH_LONG).show();
                return;
            }
        }
        bluetoothAdapter.cancelDiscovery();
        bdDevice = arrayListBluetoothDevices.get(arg2);
        try {
            if (bdDevice.getAddress().trim().equals(getReaderMac().trim())) {
                connect(bdDevice);
                connectedDevices.add(bdDevice);
            } else {
                Toast.makeText(Blutooth.this, getResources().getString(R.string.readerConnectPermission), Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(Blutooth.this, "Unable to connect to the bluetooth device", Toast.LENGTH_LONG).show();
            Log.e("", "", e);
        }
    }

    private BroadcastReceiver myReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent
                        .getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                boolean bluetoothDeviceNotAddedBefore = true;
                for (int i = 0; i < arrayListBluetoothDevices.size(); i++) {
                    if (device.getAddress().equals(
                            arrayListBluetoothDevices.get(i).getAddress())) {
                        bluetoothDeviceNotAddedBefore = false;
                    }
                }
                if (bluetoothDeviceNotAddedBefore == true) {
                    arrayListBluetoothDevicesName.add(device.getName()
                            + " : " + device.getAddress());
                    arrayListBluetoothDevices.add(device);
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                        Blutooth.this,
                        android.R.layout.simple_list_item_single_choice,
                        arrayListBluetoothDevicesName);
                listViewDetected.setAdapter(adapter);
            }
        }
    };

    private void connect(BluetoothDevice device) throws IOException {
        pairDevice(device);
        Tools.mmServerSocket = device
                .createInsecureRfcommSocketToServiceRecord(MY_UUID);
        Tools.mmServerSocket.connect();
        Tools.mmBluetoothDevice = device;

        Intent i = null;
        if (getIntent().getExtras().getString("type").equals("Milking")) {
            i = new Intent(Blutooth.this, Milking.class);
        } else if (getIntent().getExtras().getString("type")
                .equals("MilkingRow")) {
            i = new Intent(Blutooth.this, MilkingRowByRow.class);
        }
        i.putExtra("resume", getIntent().getExtras().getString("resume"));
        i.putExtra("milking_type",
                getIntent().getExtras().getInt("milking_type"));
        startActivity(i);
        finish();
    }

    private void pairDevice(BluetoothDevice blueToothDevice) {
        try {
            Method method = blueToothDevice.getClass().getMethod("createBond", (Class[]) null);
            method.invoke(blueToothDevice, (Object[]) null);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
