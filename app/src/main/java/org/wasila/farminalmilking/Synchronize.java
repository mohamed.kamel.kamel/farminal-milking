package org.wasila.farminalmilking;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Synchronize extends SuperActivity {
	Button milk_btn, tank_btn, Milk_Quality_btn;
	String lang;
	Context myContext = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sync_menu);
		lang = getLang();
		milk_btn = (Button) findViewById(R.id.milk_btn);
		tank_btn = (Button) findViewById(R.id.tank_btn);
		Milk_Quality_btn = (Button) findViewById(R.id.Milk_Quality_btn);
		// /////////////////////////////////////////////////////
		if (lang.equals("En")) {
			milk_btn.setText(getResources().getString(R.string.milkingScreenEn));
			tank_btn.setText(getResources().getString(
					R.string.milkingTankScreenEn));
			Milk_Quality_btn.setText(getResources().getString(
					R.string.qualityScreenEn));
		}
		// /////////////////////////////////////////////////////
		milk_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(Synchronize.this,
						SynchronizeMilkingDetails.class);
				startActivity(i);
			}
		});
		tank_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(Synchronize.this,
						SynchronizeTankValue.class);
				startActivity(i);
			}
		});
		Milk_Quality_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(Synchronize.this, SyncTesting.class);
				startActivity(i);
			}
		});
	}

}
