package org.wasila.farminalmilking;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.query.CloseableListIterator;
import org.greenrobot.greendao.query.Query;
import org.wasila.farminalmilking.caching.DaoMaster;
import org.wasila.farminalmilking.caching.DaoSession;
import org.wasila.farminalmilking.caching.DomainName;
import org.wasila.farminalmilking.caching.DomainNameDao;
import org.wasila.farminalmilking.caching.Lang;
import org.wasila.farminalmilking.caching.LangDao;
import org.wasila.farminalmilking.caching.ReaderMac;
import org.wasila.farminalmilking.caching.ReaderMacDao;
import org.wasila.farminalmilking.engine.webservice;

public class SuperActivity extends AppCompatActivity {
    public static DaoSession daoSession = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
    }

    public void openDb() {
        String dbFilePath = Environment.getExternalStorageDirectory().getPath() + "/farminal/dumb";
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(getApplicationContext(), dbFilePath, null);

        Database db = helper.getWritableDb();
        daoSession = new DaoMaster(db).newSession();
    }

    public void closeDb() {
        daoSession.clear();

        String dbFilePath = Environment.getExternalStorageDirectory().getPath() + "/farminal/dumb";
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(getApplicationContext(), dbFilePath, null);

        Database db = helper.getWritableDb();
        db.close();
    }

    public String getLang() {
        this.openDb();
        ////////////////////////////////////////
        LangDao langDao = daoSession.getLangDao();
        String lang = "";

        Query<Lang> levelQueryList = langDao.queryBuilder().build();
        for (CloseableListIterator<Lang> it = levelQueryList.listIterator(); it.hasNext(); ) {
            lang = it.next().getLang();
        }
        return lang;
    }

    public String getReaderMac() {
        this.openDb();
        ////////////////////////////////////////
        ReaderMacDao readerMacDao = daoSession.getReaderMacDao();
        String mac = "";

        Query<ReaderMac> levelQueryList = readerMacDao.queryBuilder().build();
        for (CloseableListIterator<ReaderMac> it = levelQueryList.listIterator(); it.hasNext(); ) {
            mac = it.next().getMac();
        }
        return mac;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length <= 0
                        && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    checkStoragePermission();
                } else {
                    startActivity(new Intent(getApplicationContext(), Splash.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                }
                return;
            }
            case 2: {
                if (grantResults.length <= 0
                        || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    getBluetoothPermission();
                } else {
                    startActivity(new Intent(getApplicationContext(), Blutooth.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                }
                return;
            }
        }
    }

    public boolean checkStoragePermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if ((checkSelfPermission(
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) ||
                    (checkSelfPermission(
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED)) {
                ActivityCompat.requestPermissions(SuperActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        1);
                return false;
            }
        }
        return true;
    }

    public boolean getBluetoothPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(
                        new String[]{Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN,
                                Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                        2);
                return false;
            }
        }
        return true;
    }

    public void getDomainName() {
        this.openDb();
        ////////////////////////////////////////
        DomainNameDao domainNameDao = daoSession.getDomainNameDao();

        Query<DomainName> domainNameQueryList = domainNameDao.queryBuilder().build();
        for (CloseableListIterator<DomainName> it = domainNameQueryList.listIterator(); it.hasNext(); ) {
            DomainName domainName = it.next();
            webservice.domain_name = domainName.getDomainName();
        }
    }
}
