package org.wasila.farminalmilking;

import java.io.IOException;

import org.json.JSONObject;
import org.wasila.farminalmilking.adapters.CustomHomeGridAdapter;
import org.wasila.farminalmilking.engine.API;
import org.wasila.farminalmilking.engine.Tools;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.GridView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Menu extends SuperActivity {
	@BindView(R.id.homeGridView)
	GridView homeGridView;
	String lang;
	Context myContext = null;
	
	@Override
	public void onBackPressed() {
		try {
//			if (Tools.mmServerSocket != null) {
//				Tools.mmServerSocket.close();
//			}
			closeDb();
		} catch (Exception e) {
			Log.e("", "", e);
		}
		finish();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu_layout);
		ButterKnife.bind(this);
		lang = getLang();
		// /////////////////////////////////////////////////////
		CustomHomeGridAdapter customHomeGridAdapter = new CustomHomeGridAdapter(getApplicationContext(), lang);
		homeGridView.setAdapter(customHomeGridAdapter);
		// /////////////////////////////////////////////////////
	}
}
