package org.wasila.farminalmilking;

import android.content.Context;
import android.support.multidex.MultiDexApplication;

public class FarminalApplication extends MultiDexApplication {

    private static Context context = null;

    @Override
    public void onCreate() {
        super.onCreate();
        setContext(getApplicationContext());
    }

    public static void setContext(Context context) {
        FarminalApplication.context = context;
    }

    public static Context getContext() {
        return context;
    }
}
