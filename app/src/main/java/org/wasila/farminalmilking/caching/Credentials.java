package org.wasila.farminalmilking.caching;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.Keep;
import org.greenrobot.greendao.annotation.NotNull;

@Entity(
        indexes = {
                @Index(value = "id, username, pass, privilege", unique = true)
        },
        createInDb = false
)

public class Credentials {

    @Id(autoincrement = true)
    private Long id;

    @NotNull
    private String username;
    private String pass;
    private int privilege;

    @Keep()
    public Credentials() {
    }

    public Credentials(Long id) {
        this.id = id;
    }

    @Generated(hash = 1119651615)
    public Credentials(Long id, @NotNull String username, String pass,
            int privilege) {
        this.id = id;
        this.username = username;
        this.pass = pass;
        this.privilege = privilege;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotNull
    public String getUsername() {
        return username;
    }

    public void setUsername(@NotNull String username) {
        this.username = username;
    }

    @NotNull
    public String getPass() {
        return pass;
    }

    public void setPass(@NotNull String pass) {
        this.pass = pass;
    }

    @NotNull
    public int getPrivilege() {
        return privilege;
    }

    public void setPrivilege(@NotNull int privilege) {
        this.privilege = privilege;
    }
}
