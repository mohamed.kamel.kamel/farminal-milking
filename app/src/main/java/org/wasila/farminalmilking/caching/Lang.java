package org.wasila.farminalmilking.caching;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.Keep;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;

@Entity(
        indexes = {
                @Index(value = "id, lang", unique = true)
        },
        createInDb = false
)

public class Lang {

    @Id(autoincrement = true)
    private Long id;

    @NotNull
    private String lang;

    @Keep()
    public Lang() {
    }

    public Lang(Long id) {
        this.id = id;
    }

    @Keep()
    public Lang(Long id, @NotNull String lang) {
        this.id = id;
        this.lang = lang;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotNull
    public String getLang() {
        return lang;
    }

    public void setLang(@NotNull String lang) {
        this.lang = lang;
    }
}
