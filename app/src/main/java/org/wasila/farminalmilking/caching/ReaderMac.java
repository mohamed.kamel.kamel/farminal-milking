package org.wasila.farminalmilking.caching;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.Keep;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;

@Entity(
        indexes = {
                @Index(value = "id, mac", unique = true)
        }
)

public class ReaderMac {

    @Id(autoincrement = true)
    private Long id;

    @NotNull
    private String mac;

    @Keep()
    public ReaderMac() {
    }

    public ReaderMac(Long id) {
        this.id = id;
    }

    @Keep()
    public ReaderMac(Long id, @NotNull String mac) {
        this.id = id;
        this.mac = mac;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotNull
    public String getMac() {
        return mac;
    }

    public void setMac(@NotNull String mac) {
        this.mac = mac;
    }
}
