package org.wasila.farminalmilking.caching;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.Keep;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;

@Entity(
        indexes = {
                @Index(value = "id, calf_id, milk_date_time, milk_point, milk_raw, milk_kilo", unique = true)
        }
)

public class MilkDetails {
    @Id(autoincrement = true)
    private Long id;

    @NotNull
    private String calf_id;
    private String milk_date_time;
    private String milk_point;
    private String milk_raw;
    private String milk_kilo;

    @Keep()
    public MilkDetails() {
    }

    public MilkDetails(Long id) {
        this.id = id;
    }

    @Keep()
    public MilkDetails(Long id, @NotNull String calf_id, String milk_date_time,
                       String milk_point, String milk_raw, String milk_kilo) {
        this.id = id;
        this.calf_id = calf_id;
        this.milk_date_time = milk_date_time;
        this.milk_point = milk_point;
        this.milk_raw = milk_raw;
        this.milk_kilo = milk_kilo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotNull
    public String getCalf_id() {
        return calf_id;
    }

    public void setCalf_id(@NotNull String calf_id) {
        this.calf_id = calf_id;
    }

    @NotNull
    public String getMilk_date_time() {
        return milk_date_time;
    }

    public void setMilk_date_time(@NotNull String milk_date_time) {
        this.milk_date_time = milk_date_time;
    }

    @NotNull
    public String getMilk_point() {
        return milk_point;
    }

    public void setMilk_point(@NotNull String milk_point) {
        this.milk_point = milk_point;
    }

    @NotNull
    public String getMilk_raw() {
        return milk_raw;
    }

    public void setMilk_raw(@NotNull String milk_raw) {
        this.milk_raw = milk_raw;
    }

    @NotNull
    public String getMilk_kilo() {
        return milk_kilo;
    }

    public void setMilk_kilo(@NotNull String milk_kilo) {
        this.milk_kilo = milk_kilo;
    }
}
