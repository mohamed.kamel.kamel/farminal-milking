package org.wasila.farminalmilking.caching;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.Keep;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;

@Entity(
        indexes = {
                @Index(value = "id, numberOfPoints, milkingType", unique = true)
        },
        createInDb = false
)

public class MilkingHouse {

    @Id(autoincrement = true)
    private Long id;

    @NotNull
    private String numberOfPoints;
    private String milkingType;

    @Keep()
    public MilkingHouse() {
    }

    public MilkingHouse(Long id) {
        this.id = id;
    }

    @Keep()
    public MilkingHouse(Long id, @NotNull String numberOfPoints, @NotNull String milkingType) {
        this.id = id;
        this.numberOfPoints = numberOfPoints;
        this.milkingType = milkingType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotNull
    public String getNumberOfPoints() {
        return numberOfPoints;
    }

    public void setNumberOfPoints(@NotNull String numberOfPoints) {
        this.numberOfPoints = numberOfPoints;
    }

    @NotNull
    public String getMilkingType() {
        return milkingType;
    }

    public void setMilkingType(@NotNull String milkingType) {
        this.milkingType = milkingType;
    }
}
