package org.wasila.farminalmilking.caching;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.Keep;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;

@Entity(
        indexes = {
                @Index(value = "id, test_date, fat, solid, SCC, Aflatoxin, Protein", unique = true)
        }
)

public class Testing {
    @Id(autoincrement = true)
    private Long id;

    @NotNull
    private String test_date;
    private String fat;
    private String solid;
    private String SCC;
    private String Aflatoxin;
    private String Protein;

    @Keep()
    public Testing() {
    }

    public Testing(Long id) {
        this.id = id;
    }

    @Keep()
    public Testing(Long id, @NotNull String test_date, String fat, String solid,
                   String SCC, String Aflatoxin, String Protein) {
        this.id = id;
        this.test_date = test_date;
        this.fat = fat;
        this.solid = solid;
        this.SCC = SCC;
        this.Aflatoxin = Aflatoxin;
        this.Protein = Protein;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotNull
    public String getTest_date() {
        return test_date;
    }

    public void setTest_date(@NotNull String test_date) {
        this.test_date = test_date;
    }

    @NotNull
    public String getFat() {
        return fat;
    }

    public void setFat(@NotNull String fat) {
        this.fat = fat;
    }

    @NotNull
    public String getSolid() {
        return solid;
    }

    public void setSolid(@NotNull String solid) {
        this.solid = solid;
    }

    @NotNull
    public String getSCC() {
        return SCC;
    }

    public void setSCC(@NotNull String SCC) {
        this.SCC = SCC;
    }

    @NotNull
    public String getAflatoxin() {
        return Aflatoxin;
    }

    public void setAflatoxin(@NotNull String aflatoxin) {
        Aflatoxin = aflatoxin;
    }

    @NotNull
    public String getProtein() {
        return Protein;
    }

    public void setProtein(@NotNull String protein) {
        Protein = protein;
    }
}
