package org.wasila.farminalmilking.caching;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.Keep;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;

@Entity(
        indexes = {
                @Index(value = "id, val_date, val_kilo", unique = true)
        }
)

public class TankValue {
    @Id(autoincrement = true)
    private Long id;

    @NotNull
    private String val_date;
    private String val_kilo;

    @Keep()
    public TankValue() {
    }

    public TankValue(Long id) {
        this.id = id;
    }

    @Keep()
    public TankValue(Long id, @NotNull String val_date, String val_kilo) {
        this.id = id;
        this.val_date = val_date;
        this.val_kilo = val_kilo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotNull
    public String getVal_date() {
        return val_date;
    }

    public void setVal_date(@NotNull String val_date) {
        this.val_date = val_date;
    }

    @NotNull
    public String getVal_kilo() {
        return val_kilo;
    }

    public void setVal_kilo(@NotNull String val_kilo) {
        this.val_kilo = val_kilo;
    }
}
