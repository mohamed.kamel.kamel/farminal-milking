package org.wasila.farminalmilking.caching;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.Keep;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;

@Entity(
        indexes = {
                @Index(value = "id, domainName", unique = true)
        },
        createInDb = false
)

public class DomainName {

    @Id(autoincrement = true)
    private Long id;

    @NotNull
    private String domainName;

    @Keep()
    public DomainName() {
    }

    public DomainName(Long id) {
        this.id = id;
    }

    @Keep()
    public DomainName(Long id, @NotNull String domainName) {
        this.id = id;
        this.domainName = domainName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotNull
    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(@NotNull String domainName) {
        this.domainName = domainName;
    }
}
