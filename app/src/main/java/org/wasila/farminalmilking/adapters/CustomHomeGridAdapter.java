package org.wasila.farminalmilking.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.wasila.farminalmilking.MainSettings;
import org.wasila.farminalmilking.Milking;
import org.wasila.farminalmilking.R;
import org.wasila.farminalmilking.Synchronize;
import org.wasila.farminalmilking.Tank;
import org.wasila.farminalmilking.Testing;

public class CustomHomeGridAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
    private String lang;

    public CustomHomeGridAdapter(Context context, String lang) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.lang = lang;
    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.cell_grid_view_home_page, null);
            holder = new ViewHolder();
            holder.service_type = (TextView) convertView.findViewById(R.id.service_type);
            holder.service_img = (ImageView) convertView.findViewById(R.id.service_img);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        setViewActionAndLayout(position, holder, convertView);

        return convertView;
    }

    private void setViewActionAndLayout(int position, ViewHolder holder, View convertView) {
        if (position == 0) {
            if (lang.equals("Ar")) {
                holder.service_type.setText(context.getResources().getString(R.string.milkingScreenAr));
            } else {
                holder.service_type.setText(context.getResources().getString(R.string.milkingScreenEn));
            }
            holder.service_img.setImageDrawable(context.getResources().getDrawable(R.drawable.milkingiconbtn));
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context, Milking.class).putExtra("resume", "not").setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }
            });
        } else if (position == 1) {
            if (lang.equals("Ar")) {
                holder.service_type.setText(context.getResources().getString(R.string.milkingTankScreenAr));
            } else {
                holder.service_type.setText(context.getResources().getString(R.string.milkingTankScreenEn));
            }
            holder.service_img.setImageDrawable(context.getResources().getDrawable(R.drawable.milk_tank_icon));
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context, Tank.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }
            });
        } else if (position == 2) {
            if (lang.equals("Ar")) {
                holder.service_type.setText(context.getResources().getString(R.string.qualityScreenAr));
            } else {
                holder.service_type.setText(context.getResources().getString(R.string.qualityScreenEn));
            }
            holder.service_img.setImageDrawable(context.getResources().getDrawable(R.drawable.quality_icon));
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context, Testing.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }
            });
        } else if (position == 3) {
            if (lang.equals("Ar")) {
                holder.service_type.setText(context.getResources().getString(R.string.syncAr));
            } else {
                holder.service_type.setText(context.getResources().getString(R.string.syncEn));
            }
            holder.service_img.setImageDrawable(context.getResources().getDrawable(R.drawable.wifi_5));
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context, Synchronize.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }
            });
        } else if (position == 4) {
            if (lang.equals("Ar")) {
                holder.service_type.setText(context.getResources().getString(R.string.settingsAr));
            } else {
                holder.service_type.setText(context.getResources().getString(R.string.settingsEn));
            }
            holder.service_img.setImageDrawable(context.getResources().getDrawable(R.drawable.setting));
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context, MainSettings.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }
            });
        }
    }

    class ViewHolder {
        TextView service_type;
        ImageView service_img;
    }
}
