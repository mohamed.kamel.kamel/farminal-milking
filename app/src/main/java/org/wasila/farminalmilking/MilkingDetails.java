package org.wasila.farminalmilking;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import org.wasila.farminalmilking.engine.API;
import org.wasila.farminalmilking.engine.Milking_Info;
import org.wasila.farminalmilking.engine.Tools;
import org.wasila.farminalmilking.engine.webservice;
import org.wasila.farminalmilking.engine.API_Local;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View.OnClickListener;

public class MilkingDetails extends SuperActivity {
    EditText milk_kilo_edit = null;
    Button milk_kilo_btn = null;
    LinearLayout resync;
    int pos = -1, raw = -1;
    TextView milk_raw = null, calf_id = null, cowNoTxt = null;
    Context myContext = null;
    String lang = "", readMessage = "";
    String date_time = "";
    private UUID MY_UUID = UUID
            .fromString("08f4242c-882c-4f09-839b-3546ff829367");

    @Override
    public void onBackPressed() {
        Intent i = new Intent(MilkingDetails.this, Milking.class);
        i.putExtra("resume", "resume");
        i.putExtra("milking_type",
                getIntent().getExtras().getInt("milking_type"));
        startActivity(i);
        finish();
        super.onBackPressed();
    }

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.milking_details);
        DateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.ENGLISH);
        Date date = new Date();
        date_time = format.format(date);
        lang = getLang();
        milk_kilo_edit = (EditText) findViewById(R.id.milk_kilo_edit);
        milk_kilo_btn = (Button) findViewById(R.id.milk_kilo_btn);
        milk_raw = (TextView) findViewById(R.id.milk_raw);
        calf_id = (TextView) findViewById(R.id.calf_id);
        cowNoTxt = (TextView) findViewById(R.id.cowNoTxt);
        resync = (LinearLayout) findViewById(R.id.resync);
        pos = getIntent().getExtras().getInt("pos");
        raw = getIntent().getExtras().getInt("raw");
        // //////////////////////////////////////////
        if (lang.equals("En")) {
            cowNoTxt.setText(getResources().getString(R.string.cowNoEn));
            milk_kilo_edit.setHint(getResources().getString(
                    R.string.enterMilkingAmoutEn));
            milk_raw.setText(getResources().getString(
                    R.string.noOfMilKingPointEn));
            milk_kilo_btn.setText(getResources().getString(R.string.saveEn));
        }
        // //////////////////////////////////////////
        if (raw == 0) {
            milk_raw.setText(milk_raw.getText() + " " + (pos + 1));
        } else if (raw == 1) {
            if (lang.equals("Ar")) {
                milk_raw.setText(milk_raw.getText() + " " + (pos + 1)
                        + "الصف الأول");
            } else if (lang.equals("En")) {
                milk_raw.setText(milk_raw.getText() + " " + (pos + 1)
                        + " 1st row");
            }
        } else if (raw == 2) {
            if (lang.equals("Ar")) {
                milk_raw.setText(milk_raw.getText() + " " + (pos + 1)
                        + "الصف الثانى");
            } else if (lang.equals("En")) {
                milk_raw.setText(milk_raw.getText() + " " + (pos + 1)
                        + " 2nd row");
            }
        }
        milk_kilo_btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                boolean flag = true;
                for (Milking_Info milkingInfo : Tools.MI) {
                    if (milkingInfo.get_animal_id().equals(
                            calf_id.getText().toString())) {
                        if (milkingInfo.get_milk_raw_full_path().equals(
                                milk_raw.getText().toString()) == false) {
                            flag = false;
                        }
                    }
                }
                for (Milking_Info milkingInfo : Tools.MI_Even) {
                    if (milkingInfo.get_animal_id().equals(
                            calf_id.getText().toString())) {
                        if (milkingInfo.get_milk_raw_full_path().equals(
                                milk_raw.getText().toString()) == false) {
                            flag = false;
                        }
                    }
                }
                if (flag) {
                    if (!milk_kilo_edit.getText().toString().equals("")) {
                        if (Double.parseDouble(milk_kilo_edit.getText().toString()) > 30.00) {
                            if (lang.equals("Ar")) {
                                Toast.makeText(
                                        getApplicationContext(),
                                        "غير مسموح لأضافة كمية لبن بأكثر من 30 كيلو من فضلك قم بإعادة المحاولة",
                                        Toast.LENGTH_LONG).show();
                            } else if (lang.equals("En")) {
                                Toast.makeText(
                                        getApplicationContext(),
                                        "Note that not allowed to add into milking point session more than 30K please revisit it!!",
                                        Toast.LENGTH_LONG).show();
                            }
                            return;
                        }
                        if (getIntent().getExtras().getInt("update") == -1) {
                            if (new webservice()
                                    .isNetworkAvailable(MilkingDetails.this)) {
                                new sendMilkInfo().execute();
                            } else {
                                if (calf_id.getText().toString().equals("")) {
                                    if (lang.equals("Ar")) {
                                        Toast.makeText(
                                                getApplicationContext(),
                                                getResources()
                                                        .getString(
                                                                R.string.readTagErrorAr),
                                                Toast.LENGTH_LONG).show();
                                    } else if (lang.equals("En")) {
                                        Toast.makeText(
                                                getApplicationContext(),
                                                getResources()
                                                        .getString(
                                                                R.string.readTagErrorEn),
                                                Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    System.out.println("api local");
                                    new API_Local(MilkingDetails.this, lang)
                                            .addMilk_details(calf_id.getText()
                                                            .toString(), date_time,
                                                    (pos + 1) + "", raw + "",
                                                    milk_kilo_edit.getText()
                                                            .toString());
                                    Milking_Info milk = new Milking_Info();
                                    milk.set_active(true);
                                    milk.set_process_id(999);
                                    milk.set_animal_id(calf_id.getText()
                                            .toString());
                                    milk.set_MilkinKilos(Double
                                            .parseDouble(milk_kilo_edit
                                                    .getText().toString()));
                                    milk.set_milk_raw_full_path(milk_raw
                                            .getText().toString());
                                    System.out.println("finish insertion");
                                    if (raw == 0 || raw == 1) {
                                        Tools.MI.set(pos, milk);
                                    } else {
                                        Tools.MI_Even.set(pos, milk);
                                    }
                                    milk.set_old_milk_date_time(date_time);
                                    Intent i = new Intent(MilkingDetails.this,
                                            Milking.class);
                                    i.putExtra("resume", "resume");
                                    i.putExtra("milking_type", getIntent()
                                            .getExtras().getInt("milking_type"));
                                    startActivity(i);
                                    finish();
                                }
                            }
                        } else {
                            if (new webservice()
                                    .isNetworkAvailable(MilkingDetails.this)) {
                                new sendMilkUpdate().execute();
                            } else {
                                if (calf_id.getText().toString().equals("")) {
                                    if (lang.equals("Ar")) {
                                        Toast.makeText(
                                                getApplicationContext(),
                                                getResources()
                                                        .getString(
                                                                R.string.readTagErrorAr),
                                                Toast.LENGTH_LONG).show();
                                    } else if (lang.equals("En")) {
                                        Toast.makeText(
                                                getApplicationContext(),
                                                getResources()
                                                        .getString(
                                                                R.string.readTagErrorEn),
                                                Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    if (raw == 0 || raw == 1) {
                                        new API_Local(MilkingDetails.this, lang)
                                                .updateMilk_details(
                                                        calf_id.getText()
                                                                .toString(),
                                                        date_time,
                                                        (pos + 1) + "",
                                                        raw + "",
                                                        milk_kilo_edit
                                                                .getText()
                                                                .toString(),
                                                        Tools.MI.get(pos)
                                                                .get_old_milk_date_time());
                                    } else {
                                        new API_Local(MilkingDetails.this, lang)
                                                .updateMilk_details(
                                                        calf_id.getText()
                                                                .toString(),
                                                        date_time,
                                                        (pos + 1) + "",
                                                        raw + "",
                                                        milk_kilo_edit
                                                                .getText()
                                                                .toString(),
                                                        Tools.MI_Even
                                                                .get(pos)
                                                                .get_old_milk_date_time());
                                    }
                                    Milking_Info milk = new Milking_Info();
                                    milk.set_active(true);
                                    milk.set_animal_id(calf_id.getText()
                                            .toString());
                                    milk.set_MilkinKilos(Double
                                            .parseDouble(milk_kilo_edit
                                                    .getText().toString()));
                                    if (raw == 0 || raw == 1) {
                                        Tools.MI.set(pos, milk);
                                    } else {
                                        Tools.MI_Even.set(pos, milk);
                                    }
                                    milk.set_old_milk_date_time(date_time);
                                    milk.set_milk_raw_full_path(milk_raw
                                            .getText().toString());
                                    Intent i = new Intent(MilkingDetails.this,
                                            Milking.class);
                                    i.putExtra("resume", "resume");
                                    i.putExtra("milking_type", getIntent()
                                            .getExtras().getInt("milking_type"));
                                    startActivity(i);
                                    finish();
                                }
                            }
                        }
                    }
                } else {
                    if (lang.equals("Ar")) {
                        Toast.makeText(
                                getApplicationContext(),
                                "غير مسموح لأضافة كمية لبن لنفس الماشية فى نقطتين مختلفتين فى المحلب",
                                Toast.LENGTH_LONG).show();
                    } else if (lang.equals("En")) {
                        Toast.makeText(
                                getApplicationContext(),
                                "Not allowed to add the milking amount for the same cattle in different milking points",
                                Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
        calf_id.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    InputStream mmInStream = Tools.mmServerSocket
                            .getInputStream();
                    byte[] buffer = new byte[1024]; // buffer store for the
                    // stream
                    int bytes; // bytes returned from read()
                    // Read from the InputStream
                    bytes = mmInStream.read(buffer);
                    // Send the obtained bytes to the UI activity
                    readMessage = new String(buffer, 0, bytes);
                    Toast.makeText(getApplicationContext(), readMessage,
                            Toast.LENGTH_SHORT).show();
                    calf_id.setBackgroundDrawable(null);
                    if (new webservice()
                            .isNetworkAvailable(MilkingDetails.this)) {
                        new getId().execute();
                    } else {
                        calf_id.setText(readMessage);
                    }
                } catch (IOException e) {
                    Toast.makeText(getApplicationContext(),
                            "bluetooth can't be open", Toast.LENGTH_SHORT)
                            .show();
                    Toast.makeText(getApplicationContext(), e.getMessage(),
                            Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        });
        resync.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (Tools.mmBluetoothDevice == null)
                        connect(Tools.mmBluetoothDevice);
                } catch (IOException e) {
                    Toast.makeText(getApplicationContext(), "حدث خطأ فى الربط مع القارئ أعد المحاولة", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        });
    }

    class getId extends AsyncTask<String, Void, Void> {
        String res;
        ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if (lang.equals("Ar")) {
                pDialog = new ProgressDialog(MilkingDetails.this);
                pDialog.setMessage(getResources().getString(R.string.LoadingAr));
                pDialog.show();
            } else if (lang.equals("En")) {
                pDialog = new ProgressDialog(MilkingDetails.this);
                pDialog.setMessage(getResources().getString(R.string.LoadingEn));
                pDialog.show();
            }
        }

        @Override
        protected Void doInBackground(String... params) {
            res = new API().get_ear_tag(readMessage);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            pDialog.dismiss();
            if (res.equals("0")) {
//                calf_id.setText("");
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        MilkingDetails.this);
                if (lang.equals("En")) {
                    builder.setMessage(
                            getResources().getString(R.string.errorCalfEn))
                            .setCancelable(true);
                    builder.setPositiveButton(
                            getResources().getString(R.string.OKEn),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else if (lang.equals("Ar")) {
                    builder.setMessage(
                            getResources().getString(R.string.errorCalfAr))
                            .setCancelable(true);
                    builder.setPositiveButton(
                            getResources().getString(R.string.OkAr),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            } else {
                calf_id.setText(res);
            }
        }
    }

    private void connect(BluetoothDevice device) throws IOException {
        pairDevice(device);
        Tools.mmServerSocket = device
                .createInsecureRfcommSocketToServiceRecord(MY_UUID);
        Tools.mmServerSocket.connect();
        Tools.mmBluetoothDevice = device;
    }

    private void pairDevice(BluetoothDevice blueToothDevice) {
        try {
            Method method = blueToothDevice.getClass().getMethod("createBond", (Class[]) null);
            method.invoke(blueToothDevice, (Object[]) null);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    class sendMilkInfo extends AsyncTask<String, Void, Void> {
        int res;
        ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if (lang.equals("Ar")) {
                pDialog = new ProgressDialog(MilkingDetails.this);
                pDialog.setMessage(getResources().getString(R.string.LoadingAr));
                pDialog.show();
            } else if (lang.equals("En")) {
                pDialog = new ProgressDialog(MilkingDetails.this);
                pDialog.setMessage(getResources().getString(R.string.LoadingEn));
                pDialog.show();
            }
        }

        @Override
        protected Void doInBackground(String... params) {
            if (calf_id.getText().toString().equals("")) {
                res = -2;
            } else {
                res = new API()
                        .insert_milking(calf_id.getText().toString(), pos + 1,
                                raw, Double.parseDouble(milk_kilo_edit
                                        .getText().toString()), date_time);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            pDialog.dismiss();
            if (res == -2) {
                if (lang.equals("Ar")) {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.readTagErrorAr),
                            Toast.LENGTH_LONG).show();
                } else if (lang.equals("En")) {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.readTagErrorEn),
                            Toast.LENGTH_LONG).show();
                }
            } else if (res == -1) {
                if (lang.equals("Ar")) {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.errorInputAr),
                            Toast.LENGTH_LONG).show();
                } else if (lang.equals("En")) {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.errorInputEn),
                            Toast.LENGTH_LONG).show();
                }
            } else {
                Milking_Info milk = new Milking_Info();
                milk.set_active(true);
                milk.set_animal_id(calf_id.getText().toString());
                milk.set_MilkinKilos(Double.parseDouble(milk_kilo_edit
                        .getText().toString()));
                milk.set_process_id(res);
                milk.set_milk_raw_full_path(milk_raw.getText().toString());
                if (raw == 0 || raw == 1) {
                    Tools.MI.set(pos, milk);
                } else {
                    Tools.MI_Even.set(pos, milk);
                }
                Intent i = new Intent(MilkingDetails.this, Milking.class);
                i.putExtra("resume", "resume");
                i.putExtra("milking_type",
                        getIntent().getExtras().getInt("milking_type"));
                startActivity(i);
                finish();
            }
        }
    }

    class sendMilkUpdate extends AsyncTask<String, Void, Void> {
        int res;
        ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if (lang.equals("Ar")) {
                pDialog = new ProgressDialog(MilkingDetails.this);
                pDialog.setMessage(getResources().getString(R.string.LoadingAr));
                pDialog.show();
            } else if (lang.equals("En")) {
                pDialog = new ProgressDialog(MilkingDetails.this);
                pDialog.setMessage(getResources().getString(R.string.LoadingEn));
                pDialog.show();
            }
        }

        @Override
        protected Void doInBackground(String... params) {
            if (raw == 0 || raw == 1) {
                res = new API()
                        .update_milking(calf_id.getText().toString(), Tools.MI
                                .get(pos).get_process_id(), Double
                                .parseDouble(milk_kilo_edit.getText()
                                        .toString()));
            } else {
                res = new API()
                        .update_milking(calf_id.getText().toString(),
                                Tools.MI_Even.get(pos).get_process_id(), Double
                                        .parseDouble(milk_kilo_edit.getText()
                                                .toString()));
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            pDialog.dismiss();
            if (res == -1) {
                if (lang.equals("Ar")) {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.errorInputAr),
                            Toast.LENGTH_LONG).show();
                } else if (lang.equals("En")) {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.errorInputEn),
                            Toast.LENGTH_LONG).show();
                }
            } else {
                Milking_Info milk = new Milking_Info();
                milk.set_active(true);
                milk.set_animal_id(calf_id.getText().toString());
                milk.set_MilkinKilos(Double.parseDouble(milk_kilo_edit
                        .getText().toString()));
                milk.set_process_id(res);
                milk.set_milk_raw_full_path(milk_raw.getText().toString());
                if (raw == 0 || raw == 1) {
                    Tools.MI.set(pos, milk);
                } else {
                    Tools.MI_Even.set(pos, milk);
                }
                Intent i = new Intent(MilkingDetails.this, Milking.class);
                i.putExtra("resume", "resume");
                i.putExtra("milking_type",
                        getIntent().getExtras().getInt("milking_type"));
                startActivity(i);
                finish();
            }
        }
    }
}
