package org.wasila.farminalmilking;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Milking_Type extends SuperActivity {
	Button pointbypoint,rowbyrow;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.milking_type);
		pointbypoint = (Button) findViewById(R.id.pointbypoint);
		pointbypoint.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(Milking_Type.this, Milking.class);
				i.putExtra("resume", "not");
				startActivity(i);
			}
		});
		rowbyrow = (Button) findViewById(R.id.rowbyrow);
		rowbyrow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(Milking_Type.this, MilkingRowByRow.class);
				i.putExtra("resume", "not");
				startActivity(i);
			}
		});
	}

}
