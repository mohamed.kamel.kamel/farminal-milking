package org.wasila.farminalmilking;

import org.wasila.farminalmilking.engine.Milking_Info;
import org.wasila.farminalmilking.engine.RowAdapter;
import org.wasila.farminalmilking.engine.RowAdapterRow;
import org.wasila.farminalmilking.engine.Tools;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;

public class MilkingDetailsRow extends SuperActivity {
	ImageView logo = null;
	boolean flag_milking = false;
	ListView milkingListView = null;
	int number_of_points = -1;
	int row_no, milking_type;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.milking_list);
		logo = (ImageView) findViewById(R.id.logo);
		milkingListView = (ListView) findViewById(R.id.milkingListView);
		flag_milking = getIntent().getExtras().getBoolean("is_milking");
		row_no = getIntent().getExtras().getInt("raw");
		milking_type = getIntent().getExtras().getInt("milking_type");
		// ///////////////////////////////////////////////////////////
		logo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(MilkingDetailsRow.this, Blutooth.class);
				i.putExtra("resume", getIntent().getExtras()
						.getString("resume"));
				i.putExtra("type", "MilkingRow");
				i.putExtra("milking_type",milking_type);
				startActivity(i);
			}
		});
		if (milking_type == 2) {
			RowAdapterRow adapter = new RowAdapterRow(MilkingDetailsRow.this,
					Tools.MI_Even, flag_milking);
			milkingListView.setAdapter(adapter);
		} else if (milking_type == 1 || milking_type == 0) {
			RowAdapterRow adapter = new RowAdapterRow(MilkingDetailsRow.this,
					Tools.MI, flag_milking);
			milkingListView.setAdapter(adapter);
		}
	}

}
