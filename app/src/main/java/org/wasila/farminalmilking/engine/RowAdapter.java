package org.wasila.farminalmilking.engine;

import java.util.ArrayList;

import org.wasila.farminalmilking.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class RowAdapter extends ArrayAdapter<Milking_Info> {
	private Context context;
	private ArrayList<Milking_Info> values;

	public RowAdapter(Context context, ArrayList<Milking_Info> values) {
		super(context, R.layout.raw, values);
		this.context = context;
		this.values = values;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.raw, parent, false);
		TextView no_of_milking_point = (TextView) rowView
				.findViewById(R.id.no_of_milking_point);
		no_of_milking_point.setText((position + 1) + "");
		if (values.get(position).get_active()) {
			ImageView image_milking = (ImageView) rowView
					.findViewById(R.id.image_milking);
			image_milking.setImageResource(R.drawable.milkactive);
			ImageView tank_image_milking = (ImageView) rowView
					.findViewById(R.id.tank_image_milking);
			tank_image_milking.setImageResource(R.drawable.milk_tank_active);
			TextView kilos_of_milking = (TextView) rowView
					.findViewById(R.id.kilos_of_milking);
			kilos_of_milking.setText(values.get(position).get_MilkinKilos()
					+ "");
		}
		return rowView;
	}
}
