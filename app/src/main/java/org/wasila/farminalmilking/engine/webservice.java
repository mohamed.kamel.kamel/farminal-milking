package org.wasila.farminalmilking.engine;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.URLDecoder;
import java.net.URLEncoder;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;


import org.wasila.farminalmilking.FarminalApplication;
import org.wasila.farminalmilking.engine.network.PersistentCookieStore;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.CookieJar;
import okhttp3.FormBody;
import okhttp3.JavaNetCookieJar;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class webservice {

	private OkHttpClient httpclient = null;

	public static String domain_name = "";
	public static String default_domain_name = "http://192.168.1.6/farminal/";

	public webservice(){
		CookieHandler cookieHandler = new CookieManager(
				new PersistentCookieStore(FarminalApplication.getContext()), CookiePolicy.ACCEPT_ALL);

		httpclient = new OkHttpClient.Builder()
				.cookieJar(new JavaNetCookieJar(cookieHandler))
				.build();
	}

	public boolean isNetworkAvailable(Activity activity) {
		ConnectivityManager connectivity = (ConnectivityManager) activity
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity == null) {
			return false;
		} else {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null) {
				for (int i = 0; i < info.length; i++) {
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public String postRequest(String url, FormBody.Builder formBodyBuilder) throws IOException {
		FormBody formBody = formBodyBuilder.build();
		Request request = new Request.Builder()
				.url(url)
				.post(formBody)
				.build();
		Response response = httpclient.newCall(request).execute();
		return response.body().string();
	}

	public String getRequest(String url) throws IOException {
		Request request = new Request.Builder()
				.url(url)
				.build();
		Response response = httpclient.newCall(request).execute();
		return response.body().string();
	}

	public boolean executeMultipartPost(String url, String FName, Bitmap bitmap) {
		try {

			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			bitmap.compress(CompressFormat.JPEG, 50, bos);
			byte[] data = bos.toByteArray();

			RequestBody requestBody = new MultipartBody.Builder()
					.setType(MultipartBody.FORM)
					.addFormDataPart("fupload", FName,
							RequestBody.create(MediaType.parse("image/jpg"), data))
					.build();

			Request request = new Request.Builder()
					.url(url)
					.post(requestBody)
					.build();

			httpclient.newCall(request).enqueue(new Callback() {

				@Override
				public void onFailure(Call call, IOException e) {

				}

				@Override
				public void onResponse(Call call, Response response) throws IOException {

				}
			});

			return true;
		} catch (Exception ex) {
		}
		return false;

	}

	public static String url_encoding(String in)
			throws UnsupportedEncodingException {
		return URLEncoder.encode(in, "utf-8");
	}

	public static String url_decoding(String in)
			throws UnsupportedEncodingException {
		return URLDecoder.decode(in, "utf-8");
	}
}
