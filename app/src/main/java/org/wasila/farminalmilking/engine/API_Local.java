package org.wasila.farminalmilking.engine;

import java.util.ArrayList;

import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.query.CloseableListIterator;
import org.greenrobot.greendao.query.Query;
import org.greenrobot.greendao.query.QueryBuilder;
import org.wasila.farminalmilking.R;
import org.wasila.farminalmilking.caching.DaoMaster;
import org.wasila.farminalmilking.caching.DaoSession;
import org.wasila.farminalmilking.caching.MilkDetails;
import org.wasila.farminalmilking.caching.MilkDetailsDao;
import org.wasila.farminalmilking.caching.TankValue;
import org.wasila.farminalmilking.caching.TankValueDao;
import org.wasila.farminalmilking.caching.Testing;
import org.wasila.farminalmilking.caching.TestingDao;

import android.app.Activity;
import android.os.Environment;
import android.widget.Toast;


public class API_Local {
    Activity act = null;
    String lang = "";
    private static DaoSession daoSession = null;

    public API_Local(Activity act, String lang) {
        this.act = act;
        this.lang = lang;
        openDb();
    }

    private void openDb() {
        String dbFilePath = Environment.getExternalStorageDirectory().getPath() + "/farminal/dumb1";
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(act.getApplicationContext(), dbFilePath, null);

        Database db = helper.getWritableDb();
        daoSession = new DaoMaster(db).newSession();
    }

    public void deleteMilk_details(String milkDateTime) {
        MilkDetailsDao milkDetailsDao = daoSession.getMilkDetailsDao();

        QueryBuilder<MilkDetails> qb = milkDetailsDao.queryBuilder();
        qb.where(MilkDetailsDao.Properties.Milk_date_time.eq(milkDateTime));
        Query<MilkDetails> calfQueryList = qb.build();

        for (CloseableListIterator<MilkDetails> it = calfQueryList.listIterator(); it.hasNext(); ) {
            MilkDetails milkDetails = it.next();
            milkDetailsDao.delete(milkDetails);
        }
    }

    public void addMilk_details(String calf_id, String milk_date_time, String milk_point, String milk_raw, String milk_kilo) {

        MilkDetails milkDetails = new MilkDetails(null, calf_id, milk_date_time, milk_point, milk_raw, milk_kilo);
        MilkDetailsDao milkDetailsDao = daoSession.getMilkDetailsDao();
        milkDetailsDao.save(milkDetails);

        if (lang.equals("En")) {
            Toast.makeText(act.getApplicationContext(),
                    act.getResources().getString(R.string.savedataEn),
                    Toast.LENGTH_LONG).show();
        } else if (lang.equals("Ar")) {
            Toast.makeText(act.getApplicationContext(),
                    act.getResources().getString(R.string.savedataAr),
                    Toast.LENGTH_LONG).show();
        }
    }

    public void updateMilk_details(String calf_id, String milk_date_time,
                                   String milk_point, String milk_raw, String milk_kilo,
                                   String old_milk_date_time) {
        MilkDetailsDao milkDetailsDao = daoSession.getMilkDetailsDao();

        QueryBuilder<MilkDetails> qb = milkDetailsDao.queryBuilder();
        qb.where(MilkDetailsDao.Properties.Milk_date_time.eq(old_milk_date_time));
        Query<MilkDetails> calfQueryList = qb.build();

        for (CloseableListIterator<MilkDetails> it = calfQueryList.listIterator(); it.hasNext(); ) {
            MilkDetails oldMilkDetails = it.next();
            oldMilkDetails.setId(oldMilkDetails.getId());
            oldMilkDetails.setCalf_id(calf_id);
            oldMilkDetails.setMilk_date_time(milk_date_time);
            oldMilkDetails.setMilk_point(milk_point);
            oldMilkDetails.setMilk_raw(milk_raw);
            oldMilkDetails.setMilk_kilo(milk_kilo);
            milkDetailsDao.insertOrReplace(oldMilkDetails);
        }

        if (lang.equals("En")) {
            Toast.makeText(act.getApplicationContext(),
                    act.getResources().getString(R.string.savedataEn),
                    Toast.LENGTH_LONG).show();
        } else if (lang.equals("Ar")) {
            Toast.makeText(act.getApplicationContext(),
                    act.getResources().getString(R.string.savedataAr),
                    Toast.LENGTH_LONG).show();
        }
    }

    public ArrayList<Milking_Info> selectMilk_details() {
        MilkDetailsDao milkDetailsDao = daoSession.getMilkDetailsDao();
        ArrayList<Milking_Info> milking_arr = new ArrayList<Milking_Info>();
        Query<MilkDetails> levelQueryList = milkDetailsDao.queryBuilder().build();

        for (CloseableListIterator<MilkDetails> it = levelQueryList.listIterator(); it.hasNext(); ) {
            MilkDetails c = it.next();
            Milking_Info an = new Milking_Info();
            an.set_animal_id(c.getCalf_id());
            an.set_milk_date_time(c.getMilk_date_time());
            an.set_milk_point(c.getMilk_point());
            an.set_milk_raw(c.getMilk_raw());
            an.set_MilkinKilos_str(c.getMilk_kilo());
            milking_arr.add(an);
        }
        return milking_arr;
    }

    public void deleteTank_value(String val_kilo, String milkDateTime) {
        TankValueDao tankValueDao = daoSession.getTankValueDao();

        QueryBuilder<TankValue> qb = tankValueDao.queryBuilder();
        qb.where(TankValueDao.Properties.Val_date.eq(milkDateTime),
                TankValueDao.Properties.Val_kilo.eq(val_kilo));
        Query<TankValue> tankQueryList = qb.build();

        for (CloseableListIterator<TankValue> it = tankQueryList.listIterator(); it.hasNext(); ) {
            TankValue tankValue = it.next();
            tankValueDao.delete(tankValue);
        }
    }

    public void addTank_value(String val_date, String val_kilo) {
        TankValue tankValue = new TankValue(null, val_date, val_kilo);
        TankValueDao tankValueDao = daoSession.getTankValueDao();
        tankValueDao.save(tankValue);

        if (lang.equals("En")) {
            Toast.makeText(act.getApplicationContext(),
                    act.getResources().getString(R.string.savedataEn),
                    Toast.LENGTH_LONG).show();
        } else if (lang.equals("Ar")) {
            Toast.makeText(act.getApplicationContext(),
                    act.getResources().getString(R.string.savedataAr),
                    Toast.LENGTH_LONG).show();
        }
    }

    public ArrayList<TankvalueInfo> selectTank_value() {
        TankValueDao tankValueDao = daoSession.getTankValueDao();
        Query<TankValue> tankQueryList = tankValueDao.queryBuilder().build();
        ArrayList<TankvalueInfo> Tankvalue_arr = new ArrayList<TankvalueInfo>();

        for (CloseableListIterator<TankValue> it = tankQueryList.listIterator(); it.hasNext(); ) {
            TankValue c = it.next();
            TankvalueInfo an = new TankvalueInfo();
            an.set_val_date(c.getVal_date());
            an.set_val_kilo(c.getVal_kilo());
            Tankvalue_arr.add(an);
        }
        return Tankvalue_arr;
    }

    public void deleteTesting(String milkDateTime) {
        TestingDao testingDao = daoSession.getTestingDao();

        QueryBuilder<Testing> qb = testingDao.queryBuilder();
        qb.where(TestingDao.Properties.Test_date.eq(milkDateTime));
        Query<Testing> testingQueryList = qb.build();

        for (CloseableListIterator<Testing> it = testingQueryList.listIterator(); it.hasNext(); ) {
            Testing testing = it.next();
            testingDao.delete(testing);
        }
    }

    public void addTesting(String test_date, String fat, String solid,
                           String SCC, String Aflatoxin, String Protein) {

        Testing testing = new Testing(null, test_date, fat, solid, SCC, Aflatoxin, Protein);
        TestingDao testingDao = daoSession.getTestingDao();
        testingDao.save(testing);

        if (lang.equals("En")) {
            Toast.makeText(act.getApplicationContext(),
                    act.getResources().getString(R.string.savedataEn),
                    Toast.LENGTH_LONG).show();
        } else if (lang.equals("Ar")) {
            Toast.makeText(act.getApplicationContext(),
                    act.getResources().getString(R.string.savedataAr),
                    Toast.LENGTH_LONG).show();
        }
    }

    public ArrayList<TestingInfo> selectTestingInfo() {
        TestingDao testingDao = daoSession.getTestingDao();
        Query<Testing> tankQueryList = testingDao.queryBuilder().build();
        ArrayList<TestingInfo> testing_arr = new ArrayList<TestingInfo>();

        for (CloseableListIterator<Testing> it = tankQueryList.listIterator(); it.hasNext(); ) {
            Testing c = it.next();
            TestingInfo an = new TestingInfo();
            an.set_test_date(c.getTest_date());
            an.set_fat(c.getFat());
            an.set_solid(c.getSolid());
            an.set_SCC(c.getSCC());
            an.set_Aflatoxin(c.getAflatoxin());
            an.set_Protein(c.getProtein());
            testing_arr.add(an);
        }
        return testing_arr;
    }

}
