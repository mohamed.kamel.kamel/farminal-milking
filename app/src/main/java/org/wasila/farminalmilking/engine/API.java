package org.wasila.farminalmilking.engine;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import okhttp3.FormBody;

public class API {

    public String sign_out() {
        String url = webservice.domain_name + "destroy_session.php";
        String res = null;
        try {
            res = new webservice().getRequest(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;
    }

    public String login(String uname, String pass) {
        try {
            uname = webservice.url_encoding(uname);
            pass = webservice.url_encoding(pass);
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        String url = webservice.domain_name + "login.php";
        FormBody.Builder formBody = new FormBody.Builder();
        formBody.add("user_name", uname);
        formBody.add("pass", pass);
        String res = null;
        try {
            res = new webservice().postRequest(url, formBody);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;
    }

    public String get_ear_tag(String id) {
        try {
            id = webservice.url_encoding(id);
            System.out.println(webservice.url_encoding(id));
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        String res = getTagOnline(id);

        int retry = 0;
        if (res == null) {
            retry++;
            if (retry < 3) {
                res = get_ear_tag(id);
            }
        }
        if (res == null)
            return "0";
        return res;
    }

    private String getTagOnline(String id) {
        String url = webservice.domain_name + "ear_tag.php";

        FormBody.Builder formBody = new FormBody.Builder();
        formBody.add("id", id);
        String res = null;
        try {
            res = new webservice().postRequest(url, formBody);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;
    }

    public String return_milking_type() {
        String url = webservice.domain_name + "return_milking_type.php";
        String res = null;
        try {
            res = new webservice().getRequest(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;
    }

    public int insert_milking(String calf_id, int milk_point, int milk_raw,
                              Double milk_kilo, String date_time) {
        try {
            calf_id = webservice.url_encoding(calf_id);
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }

        String res = null;
        res = insertMilkingSessionForAnimal(calf_id, milk_point, milk_raw, milk_kilo, date_time);
        int retry = 0;
        if (res == null) {
            retry++;
            if (retry < 3) {
                res =insertMilkingSessionForAnimal(calf_id, milk_point, milk_raw, milk_kilo, date_time);
            }
        }
        if (res == null)
            return -1;
        return Integer.parseInt(res);
    }

    private String insertMilkingSessionForAnimal(String calf_id, int milk_point, int milk_raw,
                                                 Double milk_kilo, String date_time) {
        String url = webservice.domain_name + "insert_milking.php";

        FormBody.Builder formBody = new FormBody.Builder();
        formBody.add("calf_id", calf_id);
        formBody.add("milk_date_time", date_time);
        formBody.add("milk_point", milk_point + "");
        formBody.add("milk_raw", milk_raw + "");
        formBody.add("milk_kilo", milk_kilo + "");
        String res = null;
        try {
            res = new webservice().postRequest(url, formBody);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;
    }

    public int update_milking(String calf_id, int process_id, Double milk_kilo) {
        String milk_date_time = "";
        try {
            calf_id = webservice.url_encoding(calf_id);
            DateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.ENGLISH);
            Date date = new Date();
            milk_date_time = format.format(date);
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        String url = webservice.domain_name + "updateMilking.php";

        FormBody.Builder formBody = new FormBody.Builder();
        formBody.add("calf_id", calf_id);
        formBody.add("milk_date_time", milk_date_time);
        formBody.add("milk_kilo", milk_kilo + "");
        formBody.add("process_id", process_id + "");
        String res = null;
        try {
            res = new webservice().postRequest(url, formBody);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Integer.parseInt(res);
    }

    public int insert_testing(String test_date, String fat, String solid,
                              String Protein, String SCC, String Aflatoxin) {
        String url = webservice.domain_name + "insert_testing.php";

        FormBody.Builder formBody = new FormBody.Builder();
        formBody.add("test_date", test_date);
        formBody.add("fat", fat);
        formBody.add("solid", solid);
        formBody.add("Protein", Protein);
        formBody.add("SCC", SCC);
        formBody.add("Aflatoxin", Aflatoxin);
        String res = null;
        try {
            res = new webservice().postRequest(url, formBody);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Integer.parseInt(res);
    }

    public int insert_tank(String val_kilo, int sell_flag) {
        String milk_date_time = "";
        DateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.ENGLISH);
        Date date = new Date();
        milk_date_time = format.format(date);
        String url = webservice.domain_name + "tank.php";

        FormBody.Builder formBody = new FormBody.Builder();
        formBody.add("val_kilo", val_kilo);
        formBody.add("sell_flag", sell_flag + "");
        formBody.add("sell_date", milk_date_time);
        formBody.add("val_date", milk_date_time);
        String res = null;
        try {
            res = new webservice().postRequest(url, formBody);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Integer.parseInt(res);
    }
}
