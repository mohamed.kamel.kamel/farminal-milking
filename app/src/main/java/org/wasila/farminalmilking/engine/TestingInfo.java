package org.wasila.farminalmilking.engine;

public class TestingInfo {
	private String test_date, fat, solid, SCC, Aflatoxin, Protein;

	public void set_test_date(String test_date) {
		this.test_date = test_date;
	}

	public String get_test_date() {
		return this.test_date;
	}

	public void set_fat(String fat) {
		this.fat = fat;
	}

	public String get_fat() {
		return this.fat;
	}

	public void set_solid(String solid) {
		this.solid = solid;
	}

	public String get_solid() {
		return this.solid;
	}

	public void set_SCC(String SCC) {
		this.SCC = SCC;
	}

	public String get_SCC() {
		return this.SCC;
	}

	public void set_Aflatoxin(String Aflatoxin) {
		this.Aflatoxin = Aflatoxin;
	}

	public String get_Aflatoxin() {
		return this.Aflatoxin;
	}

	public void set_Protein(String Protein) {
		this.Protein = Protein;
	}

	public String get_Protein() {
		return this.Protein;
	}

}

