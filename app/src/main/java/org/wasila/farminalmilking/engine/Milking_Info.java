package org.wasila.farminalmilking.engine;

public class Milking_Info {

	private boolean active = false;
	private String animal_id = "", milk_date_time = "", milk_point = "",
			milk_raw = "", milk_raw_full_path = "", milk_kilos_str = "",
			old_milk_date_time = "";
	private double milk_kilos = 0.0;
	private int process_id = -1;

	public void set_active(boolean active) {
		this.active = active;
	}

	public boolean get_active() {
		return this.active;
	}

	public void set_animal_id(String id) {
		this.animal_id = id;
	}

	public String get_animal_id() {
		return this.animal_id;
	}

	public void set_MilkinKilos(double milk_kilos) {
		this.milk_kilos = milk_kilos;
	}

	public double get_MilkinKilos() {
		return this.milk_kilos;
	}

	public void set_MilkinKilos_str(String milk_kilos) {
		this.milk_kilos_str = milk_kilos;
	}

	public String get_MilkinKilos_str() {
		return this.milk_kilos_str;
	}

	public void set_process_id(int process_id) {
		this.process_id = process_id;
	}

	public int get_process_id() {
		return this.process_id;
	}

	public void set_milk_date_time(String milk_date_time) {
		this.milk_date_time = milk_date_time;
	}

	public String get_milk_date_time() {
		return this.milk_date_time;
	}

	public void set_old_milk_date_time(String old_milk_date_time) {
		this.old_milk_date_time = old_milk_date_time;
	}

	public String get_old_milk_date_time() {
		return this.old_milk_date_time;
	}

	public void set_milk_point(String milk_point) {
		this.milk_point = milk_point;
	}

	public String get_milk_point() {
		return this.milk_point;
	}

	public void set_milk_raw(String milk_raw) {
		this.milk_raw = milk_raw;
	}

	public String get_milk_raw() {
		return this.milk_raw;
	}

	public void set_milk_raw_full_path(String milk_raw_full_path) {
		this.milk_raw_full_path = milk_raw_full_path;
	}

	public String get_milk_raw_full_path() {
		return this.milk_raw_full_path;
	}
}
