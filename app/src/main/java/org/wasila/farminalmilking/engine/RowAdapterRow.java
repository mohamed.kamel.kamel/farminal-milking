package org.wasila.farminalmilking.engine;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.wasila.farminalmilking.MilkingDetails;
import org.wasila.farminalmilking.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class RowAdapterRow extends ArrayAdapter<Milking_Info> {
	Activity context;
	private ArrayList<Milking_Info> values;
	private boolean isMilking;
	String readMessage = "";
	
	public RowAdapterRow(Activity context, ArrayList<Milking_Info> values,
			boolean isMilking) {
		super(context, R.layout.row_milking_list, values);
		this.context = context;
		this.values = values;
		this.isMilking = isMilking;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.row_milking_list, parent,
				false);
		TextView no_of_milking_point = (TextView) rowView
				.findViewById(R.id.no_of_milking_point);
		no_of_milking_point.setText((position + 1) + "");
		if (!isMilking) {
			rowView.findViewById(R.id.kilos_of_milking).setEnabled(false);
		}
		TextView calf_id = (TextView) rowView.findViewById(R.id.calf_id);
		final TextView point_milking = (TextView) rowView
				.findViewById(R.id.point_milking);
		calf_id.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
//				try {
//					InputStream mmInStream = Tools.mmServerSocket
//							.getInputStream();
//					byte[] buffer = new byte[1024]; // buffer store for the
//													// stream
//					int bytes; // bytes returned from read()
//					// Read from the InputStream
//					bytes = mmInStream.read(buffer);
//					// Send the obtained bytes to the UI activity
//					readMessage = new String(buffer, 0, bytes);
//					point_milking.setText(readMessage);
//				} catch (IOException e) {
//					Toast.makeText(context, "bluetooth can't be open",
//							Toast.LENGTH_SHORT).show();
//					Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT)
//							.show();
//					e.printStackTrace();
//				}
			}
		});
		return rowView;
	}
}
