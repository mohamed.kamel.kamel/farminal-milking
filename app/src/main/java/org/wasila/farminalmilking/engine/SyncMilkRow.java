package org.wasila.farminalmilking.engine;

import java.util.ArrayList;

import org.wasila.farminalmilking.MilkingDetails;
import org.wasila.farminalmilking.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class SyncMilkRow extends ArrayAdapter<Milking_Info> {
	private Context context;
	private ArrayList<Milking_Info> values;
	private String Lang = "";

	public SyncMilkRow(Context context, ArrayList<Milking_Info> values,
			String Lang) {
		super(context, R.layout.milking_row, values);
		this.context = context;
		this.values = values;
		this.Lang = Lang;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.milking_row, parent, false);
		// ///////////////////////////////////////////////////////////////
		TextView calf_delete_checkbox = (TextView) rowView
				.findViewById(R.id.calf_delete_checkbox);
		TextView milking_date_time = (TextView) rowView
				.findViewById(R.id.milking_date_time);
		TextView kilos_of_milking = (TextView) rowView
				.findViewById(R.id.kilos_of_milking);
		if (Lang.equals("En")) {
			calf_delete_checkbox.setText(context.getResources().getString(
					R.string.deleteEn));
		}
		milking_date_time.setText(values.get(position).get_milk_date_time());
		kilos_of_milking.setText(values.get(position).get_MilkinKilos_str());
		return rowView;
	}

}
