package org.wasila.farminalmilking.engine;

import java.util.ArrayList;

import org.wasila.farminalmilking.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class SyncTestingRow extends ArrayAdapter<TestingInfo> {
	private Context context;
	private ArrayList<TestingInfo> values;
	private String Lang = "";

	public SyncTestingRow(Context context, ArrayList<TestingInfo> values,
			String Lang) {
		super(context, R.layout.testing_row, values);
		this.context = context;
		this.values = values;
		this.Lang = Lang;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.testing_row, parent, false);
		// ///////////////////////////////////////////////////////////////
		TextView calf_delete_checkbox = (TextView) rowView
				.findViewById(R.id.calf_delete_checkbox);
		TextView fatText = (TextView) rowView.findViewById(R.id.fatText);
		TextView nonfatText = (TextView) rowView.findViewById(R.id.nonfatText);
		TextView ProteinText = (TextView) rowView
				.findViewById(R.id.ProteinText);
		TextView SCCText = (TextView) rowView.findViewById(R.id.SCCText);
		TextView AflatoxinText = (TextView) rowView
				.findViewById(R.id.AflatoxinText);
		TextView DateTestText = (TextView) rowView
				.findViewById(R.id.DateTestText);
		if (Lang.equals("En")) {
			calf_delete_checkbox.setText(context.getResources().getString(
					R.string.deleteEn));
			fatText.setText(context.getResources().getString(R.string.fatEn));
			nonfatText.setText(context.getResources().getString(
					R.string.SolidsEn));
			ProteinText.setText(context.getResources().getString(
					R.string.ProteinEn));
			SCCText.setText(context.getResources().getString(R.string.SCCEn));
			AflatoxinText.setText(context.getResources().getString(
					R.string.AflatoxinEn));
			DateTestText.setText(context.getResources().getString(
					R.string.milkTestDateEn));
		}
		fatText.setText(fatText.getText().toString() + ":"
				+ values.get(position).get_fat());
		nonfatText.setText(nonfatText.getText().toString() + ":"
				+ values.get(position).get_solid());
		ProteinText.setText(ProteinText.getText().toString() + ":"
				+ values.get(position).get_Protein());
		SCCText.setText(SCCText.getText().toString() + ":"
				+ values.get(position).get_SCC());
		AflatoxinText.setText(AflatoxinText.getText().toString() + ":"
				+ values.get(position).get_Aflatoxin());
		DateTestText.setText(DateTestText.getText().toString() + ":"
				+ values.get(position).get_test_date());
		return rowView;
	}

}

