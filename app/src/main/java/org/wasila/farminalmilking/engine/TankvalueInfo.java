package org.wasila.farminalmilking.engine;

public class TankvalueInfo {
	String val_date, val_kilo;

	public void set_val_date(String val_date) {
		this.val_date = val_date;
	}

	public String get_val_date() {
		return this.val_date;
	}

	public void set_val_kilo(String val_kilo) {
		this.val_kilo = val_kilo;
	}

	public String get_val_kilo() {
		return this.val_kilo;
	}
}
