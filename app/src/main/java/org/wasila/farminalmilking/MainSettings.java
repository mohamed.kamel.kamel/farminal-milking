package org.wasila.farminalmilking;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.wasila.farminalmilking.caching.DomainName;
import org.wasila.farminalmilking.caching.DomainNameDao;
import org.wasila.farminalmilking.engine.webservice;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainSettings extends SuperActivity {
    @BindView(R.id.ipEditTxt)
    EditText ipEditTxt;
    @BindView(R.id.saveBtn)
    Button saveBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_settings);
        ButterKnife.bind(this);
    }

    public void handleSaveIp(View view) {
        String ip = ipEditTxt.getText().toString();
        if (!TextUtils.isEmpty(ip)) {
            webservice.domain_name = ip;
            DomainName domainName = new DomainName(null, ip);
            DomainNameDao domainNameDao = daoSession.getDomainNameDao();
            domainNameDao.save(domainName);
            Intent i = new Intent(MainSettings.this, Login.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            finish();
        } else {
            Toast.makeText(getApplicationContext(), "Please enter the server ip first", Toast.LENGTH_LONG).show();
        }
    }
}
