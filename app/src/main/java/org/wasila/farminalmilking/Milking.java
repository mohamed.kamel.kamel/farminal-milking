package org.wasila.farminalmilking;

import java.util.ArrayList;

import org.wasila.farminalmilking.engine.Milking_Info;
import org.wasila.farminalmilking.engine.RowAdapter;
import org.wasila.farminalmilking.engine.Tools;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;

public class Milking extends SuperActivity implements OnItemClickListener {
	ImageView logo = null, reader_logo = null;
	boolean flag = false;
	ListView milkingListView = null, milkingListVieweven = null;
	int number_of_points = Tools.number_of_points,
			milking_type = Tools.milking_type, total_animals_milked = 0,
			total_animals = 0;
	Context myContext = null;
	String lang = "";
	int REQUEST_ENABLE_BT = 5;
	ArrayList<String> mArrayAdapter = new ArrayList<String>();

	@Override
	public void onBackPressed() {
		AlertDialog.Builder builder = new AlertDialog.Builder(Milking.this);
		if (lang.equals("En")) {
			builder.setMessage(getResources().getString(R.string.finishEn))
					.setCancelable(true);
		} else if (lang.equals("Ar")) {
			builder.setMessage(getResources().getString(R.string.finishAr))
					.setCancelable(true);
		}
		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
				Intent i = new Intent(Milking.this, Menu.class);
				startActivity(i);
				Milking.this.finish();
			}
		}).setNegativeButton("No", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position,
			long arg3) {
		Intent i = new Intent(Milking.this, MilkingDetails.class);
		int raw = -1;
		if (milkingListView == arg0) {
			i.putExtra("pos", position);
			if (milkingListVieweven.getVisibility() == View.GONE) {
				raw = 0;
			} else {
				raw = 1;
			}
			i.putExtra("update", Tools.MI.get(position).get_process_id());
		} else if (milkingListVieweven == arg0) {
			i.putExtra("pos", position);
			raw = 2;
			i.putExtra("update", Tools.MI_Even.get(position).get_process_id());
		}
		i.putExtra("raw", raw);
		i.putExtra("milking_type", milking_type);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(i);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.milking);

		lang = getLang();
		logo = (ImageView) findViewById(R.id.logo);
		reader_logo = (ImageView) findViewById(R.id.readerLogo);
		reader_logo.setBackgroundResource(R.drawable.gif);
		milkingListView = (ListView) findViewById(R.id.milkingListView);
		milkingListVieweven = (ListView) findViewById(R.id.milkingListVieweven);
		milkingListView.setOnItemClickListener(Milking.this);
		milkingListVieweven.setOnItemClickListener(Milking.this);
		findViewById(R.id.cow1).setVisibility(View.GONE);
		findViewById(R.id.cow2).setVisibility(View.GONE);
		findViewById(R.id.milk1).setVisibility(View.GONE);
		findViewById(R.id.milk2).setVisibility(View.GONE);
		// //////////////////////////////////////////
		if (!getIntent().getExtras().getString("resume").equals("resume")) {
			Tools.MI.clear();
			Tools.MI_Even.clear();
			draw_layout();
		} else {
			milking_type = getIntent().getExtras().getInt("milking_type");
			if (milking_type == 2) {
				RowAdapter adapter = new RowAdapter(Milking.this, Tools.MI);
				milkingListView.setAdapter(adapter);
				adapter = new RowAdapter(Milking.this, Tools.MI_Even);
				milkingListVieweven.setAdapter(adapter);
			} else {
				milkingListVieweven.setVisibility(View.GONE);
				RowAdapter adapter = new RowAdapter(Milking.this, Tools.MI);
				milkingListView.setAdapter(adapter);
			}
		}
		logo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(Milking.this, Blutooth.class);
				i.putExtra("resume", getIntent().getExtras()
						.getString("resume"));
				i.putExtra("type", "Milking");
				i.putExtra("milking_type",
						getIntent().getExtras().getInt("milking_type"));
				startActivity(i);
			}
		});
		if (Tools.mmServerSocket != null) {
			AnimationDrawable frameAnimation = (AnimationDrawable) reader_logo
					.getBackground();
			frameAnimation.start();
		} else {
			Intent i = new Intent(Milking.this, Blutooth.class);
			i.putExtra("resume", getIntent().getExtras().getString("resume"));
			i.putExtra("type", "Milking");
			i.putExtra("milking_type",
					getIntent().getExtras().getInt("milking_type"));
			startActivity(i);
		}
	}

	public void draw_layout() {
		if (milking_type == 2) {
			for (int i = 0; i < number_of_points; i++) {
				Milking_Info milk = new Milking_Info();
				milk.set_active(false);
				Tools.MI.add(milk);
			}
			RowAdapter adapter = new RowAdapter(Milking.this, Tools.MI);
			milkingListView.setAdapter(adapter);
			for (int i = 0; i < number_of_points; i++) {
				Milking_Info milk = new Milking_Info();
				milk.set_active(false);
				Tools.MI_Even.add(milk);
			}
			adapter = new RowAdapter(Milking.this, Tools.MI_Even);
			milkingListVieweven.setAdapter(adapter);
		} else {
			milkingListVieweven.setVisibility(View.GONE);
			for (int i = 0; i < number_of_points; i++) {
				Milking_Info milk = new Milking_Info();
				milk.set_active(false);
				Tools.MI.add(milk);
			}
			RowAdapter adapter = new RowAdapter(Milking.this, Tools.MI);
			milkingListView.setAdapter(adapter);
		}
	}

}
