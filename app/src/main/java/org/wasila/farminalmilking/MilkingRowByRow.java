package org.wasila.farminalmilking;

import java.util.ArrayList;

import org.wasila.farminalmilking.engine.Milking_Info;
import org.wasila.farminalmilking.engine.RowAdapter;
import org.wasila.farminalmilking.engine.Tools;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

public class MilkingRowByRow extends SuperActivity {

	ImageView logo = null, reader_logo = null;
	ImageView cow1, milk1, cow2, milk2;
	boolean flag = false;
	ListView milkingListView = null, milkingListVieweven = null;
	int number_of_points = Tools.number_of_points,
			milking_type = Tools.milking_type, total_animals_milked = 0,
			total_animals = 0;
	Context myContext = null;
	String lang = "";
	int REQUEST_ENABLE_BT = 5;
	ArrayList<String> mArrayAdapter = new ArrayList<String>();

	@Override
	public void onBackPressed() {
		AlertDialog.Builder builder = new AlertDialog.Builder(
				MilkingRowByRow.this);
		if (lang.equals("En")) {
			builder.setMessage(getResources().getString(R.string.finishEn))
					.setCancelable(true);
		} else if (lang.equals("Ar")) {
			builder.setMessage(getResources().getString(R.string.finishAr))
					.setCancelable(true);
		}
		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
				Intent i = new Intent(MilkingRowByRow.this, Menu.class);
				startActivity(i);
				MilkingRowByRow.this.finish();
			}
		}).setNegativeButton("No", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.milking);
		lang = getLang();
		logo = (ImageView) findViewById(R.id.logo);
		reader_logo = (ImageView) findViewById(R.id.readerLogo);
		reader_logo.setBackgroundResource(R.drawable.gif);
		milkingListView = (ListView) findViewById(R.id.milkingListView);
		milkingListVieweven = (ListView) findViewById(R.id.milkingListVieweven);
		cow1 = (ImageView) findViewById(R.id.cow1);
		milk1 = (ImageView) findViewById(R.id.milk1);
		cow2 = (ImageView) findViewById(R.id.cow2);
		milk2 = (ImageView) findViewById(R.id.milk2);
		// //////////////////////////////////////////
		if (!getIntent().getExtras().getString("resume").equals("resume")) {
			Tools.MI.clear();
			Tools.MI_Even.clear();
			draw_layout();
		} else {
			milking_type = getIntent().getExtras().getInt("milking_type");
			Toast.makeText(getApplicationContext(), milking_type + "",
					Toast.LENGTH_LONG).show();
			if (milking_type == 2) {
				RowAdapter adapter = new RowAdapter(MilkingRowByRow.this,
						Tools.MI);
				milkingListView.setAdapter(adapter);
				adapter = new RowAdapter(MilkingRowByRow.this, Tools.MI_Even);
				milkingListVieweven.setAdapter(adapter);
			} else {
				milkingListVieweven.setVisibility(View.GONE);
				cow2.setVisibility(View.GONE);
				milk2.setVisibility(View.GONE);
				RowAdapter adapter = new RowAdapter(MilkingRowByRow.this,
						Tools.MI);
				milkingListView.setAdapter(adapter);
			}
			BtnsAction();
		}
		logo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(MilkingRowByRow.this, Blutooth.class);
				i.putExtra("resume", getIntent().getExtras()
						.getString("resume"));
				i.putExtra("type", "MilkingRow");
				i.putExtra("milking_type",
						getIntent().getExtras().getInt("milking_type"));
				startActivity(i);
			}
		});
		if (Tools.mmServerSocket != null) {
			AnimationDrawable frameAnimation = (AnimationDrawable) reader_logo
					.getBackground();
			frameAnimation.start();
		} else {
			Intent i = new Intent(MilkingRowByRow.this, Blutooth.class);
			i.putExtra("resume", getIntent().getExtras().getString("resume"));
			i.putExtra("type", "MilkingRow");
			i.putExtra("milking_type",
					getIntent().getExtras().getInt("milking_type"));
			startActivity(i);
		}
	}

	public void draw_layout() {
		if (milking_type == 2) {
			for (int i = 0; i < number_of_points; i++) {
				Milking_Info milk = new Milking_Info();
				milk.set_active(false);
				Tools.MI.add(milk);
			}
			RowAdapter adapter = new RowAdapter(MilkingRowByRow.this, Tools.MI);
			milkingListView.setAdapter(adapter);
			for (int i = 0; i < number_of_points; i++) {
				Milking_Info milk = new Milking_Info();
				milk.set_active(false);
				Tools.MI_Even.add(milk);
			}
			adapter = new RowAdapter(MilkingRowByRow.this, Tools.MI_Even);
			milkingListVieweven.setAdapter(adapter);
		} else {
			milkingListVieweven.setVisibility(View.GONE);
			for (int i = 0; i < number_of_points; i++) {
				Milking_Info milk = new Milking_Info();
				milk.set_active(false);
				Tools.MI.add(milk);
			}
			RowAdapter adapter = new RowAdapter(MilkingRowByRow.this, Tools.MI);
			milkingListView.setAdapter(adapter);
		}
		BtnsAction();
	}

	public void execute_intent(String type, boolean is_milking) {
		Intent i = new Intent(MilkingRowByRow.this, MilkingDetailsRow.class);
		int raw = -1;
		if (type.equals("cow1")) {
			if (milkingListVieweven.getVisibility() == View.GONE) {
				raw = 0;
			} else {
				raw = 1;
			}
		} else if (type.equals("cow2")) {
			raw = 2;
		}
		i.putExtra("is_milking", is_milking);
		i.putExtra("raw", raw);
		i.putExtra("milking_type", milking_type);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(i);
	}

	public void BtnsAction() {
		cow1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				execute_intent("cow1", false);
			}
		});
		cow2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				execute_intent("cow2", false);
			}
		});
		milk1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				execute_intent("cow1", true);
			}
		});
		milk2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				execute_intent("milk2", true);
			}
		});
	}

}
