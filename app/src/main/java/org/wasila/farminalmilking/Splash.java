package org.wasila.farminalmilking;

import org.greenrobot.greendao.query.CloseableListIterator;
import org.greenrobot.greendao.query.Query;
import org.wasila.farminalmilking.caching.MilkingHouse;
import org.wasila.farminalmilking.caching.MilkingHouseDao;
import org.wasila.farminalmilking.engine.Tools;
import org.wasila.farminalmilking.engine.webservice;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.Toast;

public class Splash extends SuperActivity {
    Context myContext = null;
    String lang = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        try {
            if (this.checkStoragePermission()) {
                lang = getLang();
                /////////////////////////////////////////////////
                if (TextUtils.isEmpty(lang)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            Splash.this);
                    builder.setMessage(
                            "Error \n Please enter the farm setting first")
                            .setCancelable(true);
                    builder.setPositiveButton("ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    getMilkingTypeAndNumberOfPoints();

                    if (!new webservice().isNetworkAvailable(Splash.this)) {
                        if (lang.equals("En")) {
                            Toast.makeText(
                                    getApplicationContext(),
                                    "Offline mode you have to synchronize with the server later.",
                                    Toast.LENGTH_LONG).show();
                        } else if (lang.equals("Ar")) {
                            Toast.makeText(
                                    getApplicationContext(),
                                    "غير متصل بالأنترنت لذلك يتوجب عليك التزامن مع السيرفر ملحقا",
                                    Toast.LENGTH_LONG).show();
                        }
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Intent i = new Intent(Splash.this, Menu.class);
                                startActivity(i);
                                finish();
                            }
                        }, 2000);
                    } else {

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Intent i = new Intent(Splash.this, Login.class);
                                startActivity(i);
                                finish();
                            }
                        }, 2000);

                    }
                }
            }
        } catch (Exception e1) {
            AlertDialog.Builder builder = new AlertDialog.Builder(Splash.this);
            builder.setMessage("Error \n Please enter the farm setting first")
                    .setCancelable(true);
            builder.setPositiveButton("ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    public void getMilkingTypeAndNumberOfPoints() {
        this.openDb();
        ////////////////////////////////////////
        MilkingHouseDao milkingHouseDao = daoSession.getMilkingHouseDao();

        Query<MilkingHouse> levelQueryList = milkingHouseDao.queryBuilder().build();
        for (CloseableListIterator<MilkingHouse> it = levelQueryList.listIterator(); it.hasNext(); ) {
            MilkingHouse milkingHouse = it.next();
            Tools.milking_type = Integer.parseInt(milkingHouse.getMilkingType());
            Tools.number_of_points = Integer.parseInt(milkingHouse.getNumberOfPoints());
        }
    }


}
